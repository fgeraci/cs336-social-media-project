package cs336.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Team 17
 * 
 * Provides facilities for logging events into $CATALINA_HOME/logs/catalina.out
 *
 */

public class Logger {
	
	/**
	 * ERROR EVENT
	 */
	public static final String ERROR = "ERROR: ";
	/**
	 * SUCCESS EVENT
	 */
	public static final String SUCCESS = "SUCCESS: ";
	/**
	 * INFO EVENT
	 */
	public static final String INFO = "INFO: ";
	
	
	/**
	 * Logs the corresponding message, event and its date.
	 * @param message
	 * @param category
	 */
	public static void log(String message, String category) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		System.out.println(sdf.format(date)+" - " + category+message);
	}
}
