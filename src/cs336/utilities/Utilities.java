package cs336.utilities;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import cs336.beans.TopicBean;
import cs336.controller.Control;

/**
 * 
 * Provides different static utilities methods.
 * 
 * @author Team 17
 *
 */

public class Utilities {
	
	/**
	 * Separation delimiters.
	 */
	final static String delimiters = ",|-";
	
	/**
	 * Converts a list represented by a CSV string into an ArrayList<String> of tokens.
	 * @param string
	 * @return list of tokens
	 */
	
	public static ArrayList<String> getList(String s) {
		List<String> list = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(s,Utilities.delimiters,false);
		while(st.hasMoreTokens()) {
			list.add(st.nextToken());
		}
		return (ArrayList<String>)list;
	}
	
	/**
	 * 
	 * @param propertyName
	 * @return property.
	 * @throws Exception
	 */
	public static String getProperty(String propertyName) throws Exception {
		Properties properties = new Properties();
		properties.load(new FileInputStream("conf/conf.properties"));
		return properties.getProperty(propertyName);
	}
	
	/**
	 * Translates a name to and ID.
	 * @param role_name
	 * @return role id
	 */
	public static int getRoleID(String role_name) {
		
		if(role_name.equalsIgnoreCase("EndUser")) {
			return Control.END_USER;
		} else if(role_name.equalsIgnoreCase("Administrators")) {
			return Control.ADMINISTRATORS;
		} else if(role_name.equalsIgnoreCase("Moderator")) {
			return Control.MODERATOR;
		} else if(role_name.equalsIgnoreCase("Sales")) {
			return Control.SALES;
		} else {
			return -1;
		}
	}
}
