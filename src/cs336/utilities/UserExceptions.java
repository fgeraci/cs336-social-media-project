package cs336.utilities;

/**
 * 
 * Customized users nature exceptions. Extends Exception.
 * @author Team 17
 *
 */

public class UserExceptions extends Exception {
	/**
	 * No match for user on Database.
	 * @author Team 17
	 */
	public static class UserNotFoundException extends UserExceptions { }
	/**
	 * Invalid data POSTed by user.
	 * @author Team 17
	 *
	 */
	public static class UserFieldsErrorException extends UserExceptions { }
}