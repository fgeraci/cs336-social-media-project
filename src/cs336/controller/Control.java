package cs336.controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import cs336.beans.MessageBean;
import cs336.beans.PostBean;
import cs336.beans.PostCommentBean;
import cs336.beans.ProductBean;
import cs336.beans.PsdBean;
import cs336.beans.PsortBean;
import cs336.beans.ThreadBean;
import cs336.beans.TopicBean;
import cs336.beans.UserBean;
import cs336.model.SQLConnector;
import cs336.utilities.Logger;
import cs336.utilities.UserExceptions.UserFieldsErrorException;
import cs336.utilities.UserExceptions.UserNotFoundException;

/**
 * Provides the view with all the necessary mechanisms for setting and retrieving data form the model.
 * @author Team 17
 *
 */

public class Control {
	
	public static final int END_USER = 1;
	public static final int ADMINISTRATORS = 2;
	public static final int MODERATOR = 3;
	public static final int SALES = 4;
	
	/**
	 * Attempts to authenticates a user at login.
	 * @param user
	 * @return boolean
	 * @throws UserNotFoundException
	 * @throws SQLException
	 * @throws ClassNotFoundException 
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 */
	
	public static int authenticateUser(UserBean user)
			throws UserNotFoundException, SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException {
		int result = 0;
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call authenticateUser(?,?,?)}");
		cs.setString(1, user.getUserName());
		cs.setString(2, user.getPassword());
		cs.registerOutParameter(3, Types.INTEGER);
		cs.executeQuery();
		result = cs.getInt(3);
		SQLConnector.closeConnection(con);
		Logger.log("Attempt to log user: "+user.getUserName()+" code: "+result, Logger.INFO);
		if(result != 0) {
			user.setUserId(result);
			Logger.log("Successfully logged in "+user.getUserName(), Logger.SUCCESS); 
		} else {
			Logger.log("Logging attempt failed "+user.getUserName(), Logger.ERROR);
			throw new UserNotFoundException();
		}
		return result;
	}
	
	/**
	 * Attempts to register a new user on the Database.
	 * @param user
	 * @throws UserFieldsErrorException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static int registerUser(UserBean user) throws  UserFieldsErrorException, IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		String query = "{call registerUser(?,?,?,?,?,?)}";
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall(query);
		cs.setString(1, user.getFirstName());
		cs.setString(2, user.getLastName());
		cs.setString(3, user.getEmail());
		cs.setString(4, user.getPassword());
		cs.setString(5, user.getUserName());
		cs.registerOutParameter(6, Types.INTEGER);
		cs.executeQuery();
		int result = cs.getInt(6);
		SQLConnector.closeConnection(con);
		if(result == -1) {
			throw new UserFieldsErrorException();
		}
		user.setUserId(result);
		return result;
	}
	
	/**
	 * Attempts to return all data from user id user_id.
	 * @param user_id
	 * @return User instance
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 */
	public static UserBean retrieveUser(int user_id) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException, UserNotFoundException {
		UserBean user = new UserBean();
		String query = "{call retrieveUser(?)}"; // this procedure is not right
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall(query);
		cs.setInt(1, user_id);
		ResultSet rs = cs.executeQuery();
		if(rs.next()) {
			user.setUserId(user_id);
			user.setUserName(rs.getString("user_name"));
			user.setUserEmail(rs.getString("email"));
			user.setPassword(rs.getString("password"));
			user.setRanking(rs.getFloat("ranking"));
			user.setRole(rs.getString("role_name"));
		} else {
			SQLConnector.closeConnection(con);
			throw new UserNotFoundException();
		}
		SQLConnector.closeConnection(con);
		return user;
	}
	
	/**
	 * Checks against database if the user_id corresponds to an administrator.
	 * @param user_id
	 * @return
	 */
	public static boolean isManager(String role) { 
			return role.equalsIgnoreCase("Administrators");
		}
	
	/**
	 * Checks against database if the user_id corresponds to a sales manager.
	 * @param user_id
	 * @return
	 */
	public static boolean isSales(String role) { 
		return role.equalsIgnoreCase("Sales");
	}
	
	/**
	 * Checks against database if the user_id corresponds to an End User.
	 * @param user_id
	 * @return
	 */
	public static boolean isEndUser(String role) { 
		return role.equalsIgnoreCase("EndUser");
	}
	
	/**
	 * Validates the existence of a cookie. Open to a hacker's skill to steal cookies to impersonate.
	 * @param request
	 * @return true if cookie exists
	 */
	public static boolean validateCookie(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();
		if(cookies == null) return false;
		return true;
	}
	
	/**
	 * Initializes all the user's cookies.
	 * @param user
	 * @param response
	 * @throws SQLException 
	 * @throws UserNotFoundException 
	 * @throws ClassNotFoundException 
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 */
	public static synchronized void initializeCookies(UserBean user, HttpServletResponse response) 
			throws IllegalAccessException, InstantiationException, ClassNotFoundException, UserNotFoundException, SQLException {
		user = Control.retrieveUser(user.getUserId());
		for(String s : user.getProperties().keySet()) {
			Cookie c = new Cookie(s,user.getProperties().get(s));
			c.setMaxAge(60*24); // one full day of cookies!
			response.addCookie(c);
		}
	}
	
	/**
	 * Logs a user's login.
	 * @param user
	 */
	public static void countLogin(UserBean user) { }
	
	/**
	 * Check if the user is moderator.
	 * @param user
	 * @return true if yes, false otherwise.
	 */
	public static boolean isModerator(String role) {
		if(role.equalsIgnoreCase("Administrators") || role.equalsIgnoreCase("Moderator"))
			return true;
		return false;
	}
	
	
	/**
	 * Returns the specified cookie.
	 * @param cookies
	 * @param name
	 * @return cookie
	 */
	public static Cookie getCookie(Cookie[] cookies, String name) {
		for (Cookie c : cookies) {
			if (c.getName().equalsIgnoreCase(name)) return c;
		}
		return null;
	}
	
	/**
	 * Retrieve all the users.
	 * @return users
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 */
	public static ArrayList<UserBean> retrieveAllUsers() throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		ArrayList<UserBean> users = new ArrayList<UserBean>();
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call retrieveAllUsers()}");
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			UserBean user = new UserBean();
			user.setUserId(rs.getInt("user_id"));
			user.setUserName(rs.getString("user_name"));
			user.setUserFirstName(rs.getString("first_name"));
			user.setUserLastName(rs.getString("last_name"));
			user.setUserEmail(rs.getString("email"));
			user.setRanking(rs.getFloat("ranking"));
			user.setPictureURL(rs.getString("picture_URL"));
			user.setRole(rs.getString("role_name"));
			users.add(user);
		}
		SQLConnector.closeConnection(con);
		return users;
	}
	
	/**
	 * Removed user from database.
	 * @param userID
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 */
	public static void deleteUser(int userID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call deleteUser(?)}");
		cs.setInt(1, userID);
		cs.execute();
		SQLConnector.closeConnection(con);
	}
	
	/**
	 * Retrieves all the available roles' categories.
	 * @return all roles
	 * @throws SQLException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 */
	public static ArrayList<String> getAllRoles() throws SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException {
		ArrayList<String> roles = new ArrayList<String>();
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call getRoles()}");
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			roles.add(rs.getString("role_name"));
		}
		SQLConnector.closeConnection(con);
		return roles;
	}
	
	/**
	 * Updates an user's role as per administrator request.
	 * @param userID
	 * @param roleID
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void updateRole(int userID, int roleID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call modifyRole(?,?)}");
		cs.setInt(1, userID);
		cs.setInt(2, roleID);
		cs.execute();
		SQLConnector.closeConnection(con);
	}
	
	/**
	 * Updates an user's password.
	 * @param userID
	 * @param newPassword
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 */
	public static void updateUserPassword(int userID, String newPassword) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call updatePassword(?,?)}");
		cs.setInt(1, userID);
		cs.setString(2, newPassword);
		cs.execute();
		cs.close();
	}
	
	/**
	 * Updates login's count for specified user.
	 * @param userID
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void updateLogsCount(int userID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call registerLogin(?)}");
		cs.setInt(1, userID);
		cs.execute();
		cs.close();
	}
	
	/**
	 * Updates thread vote count 
	 * @param threadID
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void updateThreadVoteCount(int threadID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException{
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call updateVoteforThread(?)}");
		cs.setInt(1, threadID);
		cs.execute();
		cs.close();
	}
	
	/**
	 * Updates thread vote count 
	 * @param threadID
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void decreaseThreadVoteCount(int threadID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException{
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call decreaseVoteforThread(?)}");
		cs.setInt(1, threadID);
		cs.execute();
		cs.close();
	}
	
	public static void updatePostVoteCount(int postID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException{
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call updateVoteforPost(?)}");
		cs.setInt(1, postID);
		cs.execute();
		cs.close();
	}
	
	public static void decreasePostVoteCount(int postID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException{
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call decreaseVoteforPost(?)}");
		cs.setInt(1, postID);
		cs.execute();
		cs.close();
	}
	
	/**
	 * Retrieves all topics.
	 * @return topics
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static ArrayList<TopicBean> retrieveAllTopics(String orderBy) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		ArrayList<TopicBean> topics = new ArrayList<TopicBean>();
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call retrieveAllTopics(?)}");
		if(orderBy != null) {
			cs.setString(1, orderBy);
		} else {
			cs.setString(1, "NULL");
		}
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			TopicBean topic = new TopicBean();
			topic.setTopicID(rs.getInt("topic_id"));
			topic.setTopicName(rs.getString("topic_name"));
			topic.setThreads(rs.getInt("number_of_threads"));
			topic.setPopularity(rs.getInt("popularity"));
			topics.add(topic);
		}
		SQLConnector.closeConnection(con);
		return topics;
	}
	
	/**
	 * Creates a new topic if it does not exists already as per admin or moderator request. 
	 * @param topicName
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void addNewTopic(String topicName) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call addNewTopic(?)}");
		cs.setString(1, topicName);
		cs.execute();
		cs.close();
	}
	
	public static void addComment(int parentPost, String commentBody, int creator)throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call addNewComment(?, ?, ?)}");
		cs.setInt(1, parentPost);
		cs.setString(2, commentBody);
		cs.setInt(3, creator);
		cs.execute();
		cs.close();
	}
	
	public static int getOverallThreadVotes(int threadID)throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		int threadVotes = 0;
		CallableStatement cs = con.prepareCall("{call gettotalThreadsVotes(?)}");
		cs.setInt(1, threadID);
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			threadVotes = rs.getInt("overall_votes");
		}
		cs.close();
		return threadVotes;
	}
	
	public static int getOverallPostVotes(int postID)throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		int postVotes = 0;
		CallableStatement cs = con.prepareCall("{call gettotalPostVotes(?)}");
		cs.setInt(1, postID);
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			postVotes = rs.getInt("votes");
		}
		cs.close();
		return postVotes;
	}
	
	/**
	 * Creates a new thread if it does not exists already as per admin or moderator request. 
	 * @param threadName
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void addNewThread(String threadName, int parentTopic, int userID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call addNewThread(?, ?, ?)}");
		cs.setString(1, threadName);
		cs.setInt(2, parentTopic);
		cs.setInt(3, userID);
		cs.execute();
		cs.close();
	}
	
	/**
	 * Creates a new post if it does not exists already as per user request. 
	 * @param threadName
	 * @param parentPost
	 * @param userID
	 * @param threadID
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void addNewPost(String postBody, int parentPost, int userID, int threadID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call addNewPost(?, ?, ?, ?)}");
		cs.setString(1, postBody);
		cs.setInt(2, parentPost);
		cs.setInt(3, userID);
		cs.setInt(4, threadID);
		cs.execute();
		cs.close();
	}
	
	/**
	 * Deletes the specified topic.
	 * @param topicID
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void deleteTopic(int topicID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call deleteTopic(?)}");
		cs.setInt(1, topicID);
		cs.execute();
		cs.close();
	}
	/**
	 * Deletes the specified thread.
	 * @param threadID
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void deleteThread(int threadID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call deleteThread(?)}");
		cs.setInt(1, threadID);
		cs.execute();
		cs.close();
	}
	
	/**
	 * Deletes the specified post.
	 * @param postID
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void deletePost(int postID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call deletePost(?)}");
		cs.setInt(1, postID);
		cs.execute();
		cs.close();
	}
	/**
	 * Deletes the specified comment.
	 * @param postID
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void deleteComment(int commentID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call deleteComment(?)}");
		cs.setInt(1, commentID);
		cs.execute();
		cs.close();
	}
	
	/**
	 * Renames a topic as per an admin or moderator request.
	 * @param topicID
	 * @param newTopicName
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 */
	public static void renameTopic(int topicID, String newTopicName) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call renameTopic(?,?)}");
		cs.setInt(1, topicID);
		cs.setString(2, newTopicName);
		cs.execute();
		cs.close();
	}
	
	public static void editComment(int commentID, String newComment) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call editComment(?,?)}");
		cs.setInt(1, commentID);
		cs.setString(2, newComment);
		cs.execute();
		cs.close();
	}
	
	public static void modifyPost(int postID, String postBody) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException{
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call modifyPost(?,?)}");
		cs.setInt(1, postID);
		cs.setString(2, postBody);
		cs.execute();
		cs.close();
	}
	
	/**
	 * Renames a thread as per an admin or moderator request.
	 * @param threadID
	 * @param newThreadName
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 */
	public static void renameThread(int threadID, String newThreadName) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call renameThread(?,?)}");
		cs.setInt(1, threadID);
		cs.setString(2, newThreadName);
		cs.execute();
		cs.close();
	}
	
	/**
	 * Edit post as per user request.
	 * @param postID
	 * @param edittedPost
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 */
	public static void editPost(int postID, String edittedPost) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call editPost(?,?)}");
		cs.setInt(1, postID);
		cs.setString(2, edittedPost);
		cs.execute();
		cs.close();
	}
	
	/**
	 * Check for new messages for a specific user.
	 * @param userID
	 * @return true if new messages, false otherwise.
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static boolean hasUnreadMessages(int userID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call retrieveUnreadMessages(?)}");
		cs.setInt(1, userID);
		ResultSet rs = cs.executeQuery();
		boolean newMessages = false;
		while(rs.next()) {
			newMessages = true;
			break;
		}
		SQLConnector.closeConnection(con);
		return newMessages;
	}
	
	/**
	 * Retrieves all the messages.
	 * @param userID
	 * @param source
	 * @return messages
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static ArrayList<MessageBean> getMessages(int userID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		ArrayList<MessageBean> messages = new ArrayList<MessageBean>();
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call retrieveMessages(?)}");
		cs.setInt(1, userID);
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			MessageBean message = new MessageBean();
			message.setMessageID(rs.getInt("message_id"));
			message.setOwnerId(rs.getInt("owner_id"));
			message.setReceiverID(rs.getInt("receiver_id"));
			Date date = rs.getDate("message_date");
			SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
			message.setDate(sdf.format(date));
			message.setSubject(rs.getString("subject"));
			message.setBody(rs.getString("body"));
			message.setUnread(rs.getInt("unread"));
			message.setOwnerUserName(rs.getString("sender"));
			messages.add(message);
		}
		SQLConnector.closeConnection(con);
		return messages;
	}
	
	/**
	 * Set a message as read.
	 * @param messageID
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void markAsRead(int messageID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call markAsRead(?)}");
		cs.setInt(1, messageID);
		cs.execute();
		cs.close();
	}
	
	/**
	 * Deletes the specified message.
	 * @param messageID
	 * @throws SQLException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 */
	public static void deleteMessage(int messageID) throws SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call deleteMessage(?)}");
		cs.setInt(1, messageID);
		cs.execute();
		cs.close();
	}
	
	/**
	 * Returns a user id from the provided user name.
	 * @param userName
	 * @return user id
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static int getUserIdFromName(String userName) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		int id = -1;
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call fromUserNameToID(?)}");
		cs.setString(1, userName);
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			id = rs.getInt("user_id");
		}
		SQLConnector.closeConnection(con);
		return id;
	}
	
	/**
	 * Send a messsage between users.
	 * @param from
	 * @param to
	 * @param subject
	 * @param body
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 */
	public static void sendMessage(int from, int to, String subject, String body) throws SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call sendMessage(?,?,?,?)}");
		cs.setInt(1, from);
		cs.setInt(2, to);
		cs.setString(3, subject);
		cs.setString(4, body);
		cs.execute();
		cs.close();
	}
	
	/**
	 * Matches searchString in date, subject or body.
	 * @param userID
	 * @param searchString
	 * @return matching messages.
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static ArrayList<MessageBean> searchMessages(int userID, String searchString) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		ArrayList<MessageBean> messages = Control.getMessages(userID);
		ArrayList<MessageBean> result = new ArrayList<MessageBean>();
		searchString = searchString.replaceAll("%", " ");
		for(MessageBean m : messages) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
				Date date = sdf.parse(searchString);
				if(m.getDate().equals(sdf.format(date))) result.add(m);
			} catch (Exception e) {
				// not a date.
				if(m.getSubject().contains(searchString) || m.getBody().contains(searchString)) result.add(m);
			}
		}
		return result;
	}
	
	/**
	 * Converts a user id into a user name.
	 * @param userID
	 * @return user name
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static String getUserNameFromId(int userID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		String result = null;
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call getUserNameFromId(?)}");
		cs.setInt(1,userID);
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			result = rs.getString("user_name");
		}
		SQLConnector.closeConnection(con);
		return result;
	}
	
	/**
	 * Removes users which don't apply to the search string.
	 * @param searchString
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InstantiationException 
	 * @throws IllegalAccessException 
	 */
	public static ArrayList<UserBean> trimUsers(String searchString) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		ArrayList<UserBean> users = Control.retrieveAllUsers();
		ArrayList<UserBean> trimmedList = new ArrayList<UserBean>();
		searchString = searchString.toLowerCase();
		for(UserBean u : users) {
			String fullName = (u.getFirstName()+" "+u.getLastName()+" "+u.getUserName()).toLowerCase();
			if(fullName.contains(searchString)) {
				trimmedList.add(u);
			}
		}
		return trimmedList;
	}
	
	/**
	 * Retrieves the specified topic.
	 * @param topicID
	 * @return topic bean
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static TopicBean retrieveTopic(int topicID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		TopicBean tb = new TopicBean();
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call retrieveTopic(?)}");
		cs.setInt(1,topicID);
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			tb.setPopularity(rs.getInt("popularity"));
			tb.setThreads(rs.getInt("number_of_threads"));
			tb.setTopicID(rs.getInt("topic_id"));
			tb.setTopicName(rs.getString("topic_name"));
		}
		SQLConnector.closeConnection(con);
		return tb;
	}
	
	/**
	 * Retrieves the specified thread.
	 * @param threadID
	 * @return thread bean
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static ThreadBean retrieveThread(int threadID, int topicID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		ThreadBean tb = new ThreadBean();
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call retrievespecificThreads(?, ?)}");
		cs.setInt(1,threadID);
		cs.setInt(2, topicID);
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			tb.setCreatorID(rs.getInt("creator_id"));
			tb.setNumberOfPosts(rs.getInt("number_of_posts"));
			tb.setOverallVotes(rs.getInt("overall_votes"));
			tb.setParentTopic(rs.getInt("parent_topic_id"));
			tb.setPublishedDate(rs.getDate("published_date"));
			tb.setRank(rs.getInt("rank"));
			tb.setThreadName(rs.getString("thread_name"));
			tb.setThreadID(rs.getInt("thread_id"));
		}
		SQLConnector.closeConnection(con);
		return tb;
	}
	
	/**
	 * Retrieves all threads.
	 * @param topicID
	 * @return HashMap of all threads for a specific topic
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static HashMap<Integer,ThreadBean> getAllThreadsFromTopic(int topicID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		HashMap<Integer,ThreadBean> threads = new HashMap<Integer,ThreadBean>();
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call retrieveThreads(?)}");
		cs.setInt(1,topicID);
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			ThreadBean tb = new ThreadBean();
			tb.setThreadID(rs.getInt("thread_id"));
			tb.setCreatorID(rs.getInt("creator_id"));
			tb.setNumberOfPosts(rs.getInt("number_of_posts"));
			tb.setParentTopic(rs.getInt("parent_topic_id"));
			tb.setPublishedDate(rs.getDate("published_date"));
			tb.setThreadName(rs.getString("thread_name"));
			tb.setRank(rs.getInt("rank"));
			threads.put(tb.getThreadID(), tb);
		}
		SQLConnector.closeConnection(con);
		return threads;
	}
	
	/**
	 * Retrieves all posts for a thread.
	 * @param threadID
	 * @return HashMap of all posts for a specific thread
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	/*public static HashMap<Integer,PostBean> getAllPostsFromThread(int threadID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException{
		HashMap<Integer,PostBean> posts = new HashMap<Integer,PostBean>();
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call retrievePosts(?)}");
		cs.setInt(1,threadID);
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			PostBean pb = new PostBean();
			pb.setPostID(rs.getInt("post_id"));
			pb.setParentPost(rs.getInt("parent_post"));
			pb.setPostBody(rs.getString("body"));
			pb.setPostedDate(rs.getDate("posted_date"));
			pb.setPostOwner(rs.getInt("owner"));
			pb.setRank(rs.getInt("rank"));
			pb.setThreadId(rs.getInt("thread_id"));
			pb.setVotes(rs.getInt("votes"));
			posts.put(pb.getPostID(), pb);
		}
		SQLConnector.closeConnection(con);
		return posts;
	}*/
	public static ArrayList<PostBean> getAllPostsFromThread(int threadID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException{
		ArrayList<PostBean> posts = new ArrayList<PostBean>();
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call retrievePosts(?)}");
		cs.setInt(1,threadID);
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			PostBean pb = new PostBean();
			pb.setPostID(rs.getInt("post_id"));
			pb.setParentPost(rs.getInt("parent_post"));
			pb.setPostBody(rs.getString("body"));
			pb.setPostedDate(rs.getDate("posted_date"));
			pb.setPostOwner(rs.getInt("owner"));
			pb.setRank(rs.getInt("rank"));
			pb.setThreadId(rs.getInt("thread_id"));
			pb.setVotes(rs.getInt("votes"));
			//posts.put(pb.getPostID(), pb);
			posts.add(pb);
		}
		SQLConnector.closeConnection(con);
		return posts;
	}
	
	/**
	 * Retrieves all comments for a post.
	 * @param postID
	 * @return HashMap of all posts for a specific thread
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static ArrayList<PostCommentBean> getAllCommentsFromPost(int postID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException{
		//HashMap<Integer,PostCommentBean> posts = new HashMap<Integer,PostCommentBean>();
		ArrayList<PostCommentBean> posts = new ArrayList<PostCommentBean> ();
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call retrieveComments(?)}");
		cs.setInt(1,postID);
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			PostCommentBean pb = new PostCommentBean();
			pb.setPostCommentID(rs.getInt("post_id"));
			pb.setPostComment(rs.getString("comment"));
			pb.setPostCommentDate(rs.getString("comment_date"));
			pb.setPostCommentUser(rs.getInt("creator_id"));
			//posts.put(pb.getPostCommentID(), pb);
			posts.add(pb);
		}
		SQLConnector.closeConnection(con);
		return posts;
	}
	
	/**
	 * Retrieves all topics.
	 * @return topics
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static ArrayList<ThreadBean> retrieveAllThreads(String orderBy) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		ArrayList<ThreadBean> threads = new ArrayList<ThreadBean>();
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call retrieveAllThreads(?)}");
		if(orderBy != null) {
			cs.setString(1, orderBy);
		} else {
			cs.setString(1, "NULL");
		}
		ResultSet rs = cs.executeQuery();
		while(rs.next()) {
			ThreadBean thread = new ThreadBean();
			thread.setParentTopic(rs.getInt("parent_topic"));
			thread.setThreadName(rs.getString("thread_name"));
			thread.setNumberOfPosts(rs.getInt("number_of_posts"));
			thread.setOverallVotes(rs.getInt("overall_votes"));
			threads.add(thread);
		}
		SQLConnector.closeConnection(con);
		return threads;
	}
	
	/*Added by Harris, Places product into table*/
	public static int registerProduct(ProductBean product) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		String query = "{call registerProduct(?,?,?,?,?)}";
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall(query);
		cs.setString(1, product.getManufacturer());
		cs.setString(2, product.getPicture());
		cs.setString(3, product.getPrice());
		cs.setString(4, product.getKeywords());
		cs.registerOutParameter(5, Types.INTEGER);
		cs.executeQuery();
		int result = cs.getInt(5);
		SQLConnector.closeConnection(con);
		//if(result == -1) {
		//	throw new UserFieldsErrorException();
		//}
		product.setProductID(result);
		return result;
	}
	
	public static ArrayList<ProductBean> retrieveAllProducts() throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		try{
			ArrayList<ProductBean> products = new ArrayList<ProductBean>();
			Connection con = SQLConnector.getConnection();
			CallableStatement cs = con.prepareCall("{call retrieveAllProducts()}");
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				ProductBean product = new ProductBean();
				product.setProductID(rs.getInt("product_id"));
				product.setManufacturer(rs.getString("manufacturer"));
				product.setPicture(rs.getString("picture_url"));
				product.setKeywords(rs.getString("keywords_list"));
				product.setPrice(rs.getString("price"));
				products.add(product);
			}
			SQLConnector.closeConnection(con);
			return products;
		}
		catch(Exception e){
			Logger.log(e.getMessage(), Logger.ERROR);
		}
		return null;
	}
	
	public static void deleteProd(int prodID) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call deleteProd(?)}");
		cs.setInt(1, prodID);
		cs.execute();
		SQLConnector.closeConnection(con);
	}
	
	public static String getPrice(String adName) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException 
	{
		String price = null;
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call getPrice(?)}");
		cs.setString(1,adName);
		ResultSet rs = cs.executeQuery();
		while (rs.next()){
			price = rs.getString("price");
		}
		SQLConnector.closeConnection(con);
		return price;
	}
	
	public static int getProductID(String adName) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException 
	{
		int productID = 0;
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call getProduct(?)}");
		cs.setString(1,adName);
		ResultSet rs = cs.executeQuery();
		while (rs.next()){
			productID = rs.getInt("product_id");
		}
		SQLConnector.closeConnection(con);
		return productID;
	}
	
	public static int registerSale(int userID, int productID, String price) throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		try{
			Connection con = SQLConnector.getConnection();
			CallableStatement cs = con.prepareCall("{call registerSale(?,?,?,?,?)}");
			cs.setInt(1, userID);
			cs.setInt(2, productID);
			cs.setString(3, price);
			cs.setBoolean(4, false);
			cs.registerOutParameter(5, Types.INTEGER);
			cs.execute();
			int transact = 0;
			transact = cs.getInt(5);
			SQLConnector.closeConnection(con);
			//if(result == -1) {
			//	throw new UserFieldsErrorException();
			//}
			return transact;
		}
		catch(Exception e) {
			Logger.log(e.getMessage(), Logger.ERROR);
		}
		return -1;
		}
	
	public static void insertCustomer(int userID)throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		Connection con = SQLConnector.getConnection();
		CallableStatement cs = con.prepareCall("{call insertCustomer(?)}");
		cs.setInt(1,userID);
		cs.execute();
		SQLConnector.closeConnection(con);
	}
	public static ArrayList<PsdBean> retrieveAllPSD() throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		try{
			ArrayList<PsdBean> psds = new ArrayList<PsdBean>();
			Connection con = SQLConnector.getConnection();
			CallableStatement cs = con.prepareCall("{call retrieveAllPSD()}");
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				PsdBean psd = new PsdBean();
				psd.setUserID(rs.getInt("user_id"));
				psd.setName(rs.getString("first_name"));
				psd.setProductName(rs.getString("manufacturer"));
				psd.setPaid(rs.getString("price"));
				psds.add(psd);
			}
			SQLConnector.closeConnection(con);
			return psds;
		}
		catch(Exception e){
			Logger.log(e.getMessage(), Logger.ERROR);
		}
		return null;
	}	
	
	public static ArrayList<PsortBean> retrieveAllPsort() throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		try{
			ArrayList<PsortBean> psorts = new ArrayList<PsortBean>();
			Connection con = SQLConnector.getConnection();
			CallableStatement cs = con.prepareCall("{call retrieveAllPsort()}");
			ResultSet rs = cs.executeQuery();
			while(rs.next()) {
				PsortBean psort = new PsortBean();
				psort.setUserID(rs.getInt("user_id"));
				psort.setUserName(rs.getString("first_name"));
				psort.setProductName(rs.getString("manufacturer"));
				psort.setPrice(rs.getString("price"));
				psort.setProductID(rs.getInt("product_id"));
				psorts.add(psort);
			}
			SQLConnector.closeConnection(con);
			return psorts;
		}
		catch(Exception e){
			Logger.log(e.getMessage(), Logger.ERROR);
		}
		return null;
	}
	public static String getCustomerKeywords(int userID)throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		try{
			Connection con = SQLConnector.getConnection();
			CallableStatement cs = con.prepareCall("{call getCustomerKeywords(?)}");
			cs.setInt(1,userID);
			ResultSet rs = cs.executeQuery();
			String keywords = null;
			rs.next();
			keywords = rs.getString("keywords");
			SQLConnector.closeConnection(con);
			return keywords;
		}
		catch(Exception e){
			Logger.log(e.getMessage(), Logger.ERROR);
		}
		return null;
	}
	public static String getAdKeywords(String adName)throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		try{
			Connection con = SQLConnector.getConnection();
			CallableStatement cs = con.prepareCall("{call getAdKeywords(?)}");
			cs.setString(1,adName);
			ResultSet rs = cs.executeQuery();
			String keywords = null;
			rs.next();
			keywords = rs.getString("keywords_list");
			SQLConnector.closeConnection(con);
			return keywords;
		}catch(Exception e){
			Logger.log(e.getMessage(), Logger.ERROR);
		}
		return null;
	}
	public static void updateUserKeywords(int userID, String newKeywords)throws IllegalAccessException, InstantiationException, ClassNotFoundException, SQLException {
		try{
			Connection con = SQLConnector.getConnection();
			CallableStatement cs = con.prepareCall("{call updateUserKeywords(?,?)}");
			cs.setInt(1,userID);
			cs.setString(2, newKeywords);
			cs.executeQuery();
			SQLConnector.closeConnection(con);
		}catch(Exception e){
			Logger.log(e.getMessage(), Logger.ERROR);
		}
	}
}