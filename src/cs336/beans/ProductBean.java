package cs336.beans;

import java.util.HashMap;

/**
 * @author Team 17
 * 
 * Represents a Product object populated by either DataBase results or POST event from View.
 *
 */
public class ProductBean {

	/**
	 * The productID for the product in the database
	 */
	private int productID;
	/**
	 * The manufacturer for the product
	 */
	private String manufacturer;
	/**
	 * The name of the picture in the images folder
	 */
	private String picture;
	/**
	 * Keywords for the product
	 */
	private String keywords;
	/**
	 * The price of the product 
	 */
	private String price;
	
	public ProductBean() {
		// TODO Auto-generated constructor stub
	}

	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	/**
	 * 
	 * Creates a product bean.
	 * 
	 * @param productID
	 * @param manufacturer
	 * @param picture
	 * @param keywords
	 * @param price
	 * @return product object
	 */
	public static ProductBean createRegistrationProduct(int productID, String manfacturer,
		String picture, String keywords, String price) {
		ProductBean product = new ProductBean();
		product.productID = productID;
		product.manufacturer = manfacturer;
		product.picture = picture;
		product.keywords = keywords;
		product.price = price;
		return product;
	}
	
	/**
	 * Collection of product's properties.
	 * @return hashmap of properties
	 */
	public HashMap<String,String> getProperties() {
		HashMap<String,String> props = new HashMap<String,String>();
		props.put("product_id", Integer.toString(this.productID));
		props.put("manufacturer",this.manufacturer);
		props.put("picture", this.picture);
		props.put("keywords",this.keywords);
		// props.put("password", this.password); hidden for security
		props.put("price", this.price);
		return props;
	}
	
}	
