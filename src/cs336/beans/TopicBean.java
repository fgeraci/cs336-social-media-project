package cs336.beans;

import java.util.HashMap;


/**
 * Represents a Topic and its attributes.
 * @author Team 17
 *
 *
 *CREATE TABLE Topics
(
	topic_id int AUTO_INCREMENT,
	topic_name varchar(50) NOT NULL,
	number_of_threads int NOT NULL DEFAULT 0,
	popularity int NOT NULL DEFAULT 0,
	PRIMARY KEY (topic_id)
) Engine=InnoDB;
 */
public class TopicBean {
	
	
	/**
	 * All topic's threads.
	 */
	private HashMap<Integer,ThreadBean> topicThreads;
	/**
	 * Topic's id.
	 */
	private int topicID;
	/**
	 * Topic's name.
	 */
	private String topicName;
	/**
	 * Topic's popularity.
	 */
	private int popularity;
	/**
	 * Topic's threads number.
	 */
	private int threads;
	
	/**
	 * Constructor.
	 */
	public TopicBean() { this.topicThreads = new HashMap<Integer,ThreadBean>(); }
	
	/**
	 * Sets topic's id.
	 * @param topicID
	 */
	public void setTopicID(int topicID) { this.topicID = topicID; }
	/**
	 * Sets topic's name.
	 * @param topicName
	 */
	public void setTopicName(String topicName) { this.topicName = topicName; }
	/**
	 * Sets topic's popularity.
	 * @param popularity
	 */
	public void setPopularity(int popularity) { this.popularity = popularity; }
	/**
	 * Sets topic's number of threads.
	 * @param threads
	 */
	public void setThreads(int threads) { this.threads = threads; }
	
	/**
	 * Gets topic's id.
	 */
	public int getTopicID() { return this.topicID; }
	/**
	 * Gets topic's name.
	 */
	public String getTopicName() { return this.topicName; }
	/**
	 * Sets topic's popularity.
	 */
	public int getPopularity() { return this.popularity; }
	/**
	 * Gets topic's number of threads.
	 */
	public int getThreads() { return this.threads; }
	
	/**
	 * Returns this topic's threads.
	 * @return all threads
	 */
	public HashMap<Integer,ThreadBean> getTopicsThreads() { return this.topicThreads; }
	
	/**
	 * Set this topic's threads.
	 * @param threads
	 */
	public void setThreads(HashMap<Integer,ThreadBean> threads) { this.topicThreads = threads; }
}
