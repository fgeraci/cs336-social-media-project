package cs336.beans;

public class PsdBean {
	
	private int userID;
	
	private String name;
	
	private String paid;
	
	private String productName; 
	
	

	public PsdBean() {
		// TODO Auto-generated constructor stub
	}


	public int getUserID() {
		return userID;
	}


	public void setUserID(int userID) {
		this.userID = userID;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getPaid() {
		return paid;
	}


	public void setPaid(String paid) {
		this.paid = paid;
	}

	public static PsdBean createPSD(int userID, String name,
			String paid, String productName) {
			PsdBean psd = new PsdBean();
			psd.userID = userID;
			psd.name = name;
			psd.paid = paid;
			psd.productName = productName;
			return psd;
		}
}
