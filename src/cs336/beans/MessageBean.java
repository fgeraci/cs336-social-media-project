package cs336.beans;

public class MessageBean {
	
	private int message_id;
	private int owner_id;
	private String owner_user_name;
	private int receiver_id;
	private String receiver_user_name;
	private String subject;
	private String body;
	private String date;
	private int unread;
	private int trashed;
	
	public void setMessageID(int messageID) { this.message_id = messageID; }
	public void setOwnerId(int ownerIdD) { this.owner_id = ownerIdD; }
	public void setReceiverID(int receiverID) { this.receiver_id = receiverID; }
	public void setSubject(String subject) { this.subject = subject; }
	public void setBody(String body) { this.body = body; }
	public void setDate(String date) { this.date = date; }
	public void setUnread(int unread) { this.unread = unread; }
	public void setTrashed(int trashed) { this.trashed = trashed; }
	public void setOwnerUserName(String oUN) { this.owner_user_name = oUN; }
	public void setReceiverUserName(String rUN) { this.receiver_user_name = rUN; }
	
	public int getMessageID() { return this.message_id; }
	public int getOwnerID() { return this.owner_id; }
	public int getReceiverID() { return this.receiver_id; }
	public String getSubject() { return this.subject; }
	public String getBody() { return this.body; }
	public String getDate() { return this.date; }
	public int getUnread() { return this.unread; }
	public int getTrashed() { return this.trashed; }
	public String getOwnerUserName() { return this.owner_user_name; }
	public String getReceiverUserName() { return this.receiver_user_name; }
	
}
