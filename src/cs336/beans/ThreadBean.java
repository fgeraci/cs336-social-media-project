package cs336.beans;

import java.util.Date;

public class ThreadBean {

	/*
	thread_id int AUTO_INCREMENT,
	parent_topic_id int,
	thread_name varchar(50) NOT NULL,
	creator_id int,
	number_of_posts int NOT NULL,
	overall_votes int NOT NULL,
	published_date date NOT NULL,
	rank int NOT NULL,
	 */
	
	private int thread_id;
	private int parent_topic;
	private String thread_name;
	private int creator_id;
	private int number_of_posts;
	private int overall_votes;
	private java.sql.Date published_date;
	private int rank;
	
	public void setThreadID(int threadID) { this.thread_id=threadID;}
	public void setParentTopic(int parentTopic) { this.parent_topic = parentTopic; }
	public void setThreadName(String threadName) { this.thread_name = threadName; }
	public void setCreatorID(int creatorID) { this.creator_id = creatorID; }
	public void setNumberOfPosts(int numberOfPosts) { this.number_of_posts = numberOfPosts; }
	public void setOverallVotes(int overallVotes) { this.overall_votes = overallVotes; }
	public void setPublishedDate(java.sql.Date string) { this.published_date = string; }
	public void setRank(int rank) { this.rank = rank; }
	
	public int getThreadID() { return this.thread_id; }
	public int getParentTopic() { return this.parent_topic; }
	public String getThreadName() { return this.thread_name; }
	public int getCreatorID() { return this.creator_id; }
	public int getNumberOfPosts() { return this.number_of_posts; }
	public int getOverallVotes() { return this.overall_votes; }
	public java.sql.Date getPublishedDate() { return this.published_date; }
	public int getRank() { return this.rank; }
}
