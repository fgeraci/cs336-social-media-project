package cs336.beans;

import java.util.HashMap;

import cs336.utilities.UserExceptions.UserFieldsErrorException;

/**
 * 
 * @author Team 17
 * 
 * Represents a User object populated by either DataBase results or POST event from View.
 *
 */

public class UserBean {
	
	/**
	 * User's role in database;
	 */
	private String role;
	
	/**
	 * User's user id in data base.
	 */
	private int userId;
	/**
	 * User's first name.
	 */
	private String firstName;
	/**
	 * User's email.
	 */
	private String email;
	/**
	 * User's last name. Empty value allowed.
	 */
	private String lastName;
	/**
	 * User's user name.
	 */
	private String userName;
	/**
	 * User's password.
	 */
	private String password;
	
	/**
	 * User's ranking.
	 */
	private float ranking;
	
	/**
	 * User's picture URL.
	 */
	private String pictureURL;
	
	/**
	 * Default unique constructor.
	 */
	public UserBean() {	}
	
	/**
	 * Sets user name.
	 * @param userName
	 */
	public void setUserName(String userName) { this.userName = userName; }
	
	/**
	 * Sets user's first name.
	 * @param first_name
	 */
	public void setUserFirstName(String first_name) { this.firstName = first_name; }
	
	/**
	 * Sets user's last name.
	 * @param last_name
	 */
	public void setUserLastName(String last_name) { this.lastName = last_name; }
	
	/**
	 * Sets user's password.
	 * @param password
	 */
	public void setPassword(String password) { this.password = password; }
	
	/**
	 * Sets user's id from or to Database.
	 * @param id
	 */
	public void setUserId(int id) { this.userId = id; }
	
	/**
	 * Sets user's email address.
	 * @param email
	 */
	public void setUserEmail(String email) { this.email = email; }
	
	/**
	 * Sets user's ranking.
	 * @param ranking
	 */
	public void setRanking(float ranking) { this.ranking=ranking; }
	
	/**
	 * Sets user's role.
	 * @param role
	 */
	public void setRole(String role) { this.role = role; }
	
	/**
	 * Sets user's last name.
	 * @param last_name
	 */
	public void setLastName(String last_name) { this.lastName = last_name; }
	
	/**
	 * Set's picture URL.
	 * @param string URL
	 */
	public void setPictureURL(String URL) { this.pictureURL = URL; }
	
	/**
	 * Retrieves user's name.
	 * @return
	 */
	public String getUserName() { return this.userName; }
	/**
	 * Retrieves user's password.
	 * @return
	 */
	public String getPassword() { return this.password; }
	/**
	 * Retrieves user's email.
	 * @return
	 */
	public String getEmail() {return this.email; }
	/**
	 * Retrieves user's first name.
	 * @return
	 */
	public String getFirstName() { return this.firstName; }
	/**
	 * Retrieves user's last name.
	 * @return
	 */
	public String getLastName() { return this.lastName; }
	/**
	 * Retrieves Databases user's id.
	 * @return
	 */
	public int getUserId() { return this.userId; }
	/**
	 * Retrieves user's role's group.
	 * @return role name
	 */
	public String getRole() { return this.role; }
	
	/**
	 * Return's users ranking.
	 * @return ranking
	 */
	public String getRanking() { return Float.toString(this.ranking); }
	
	/**
	 * Overwritten toString.
	 * @return user name
	 */
	public String toString() {
		return userName;
	}
	
	/**
	 * 
	 * Creates a user bean for registration purposes from end-user's POST submitted data.
	 * 
	 * @param name
	 * @param last_name
	 * @param email
	 * @param user_id
	 * @param password
	 * @return user object
	 * @throws UserFieldsErrorException
	 */
	public static UserBean createRegistrationUser(String name, String last_name,
			String email, String user_id, String password) throws UserFieldsErrorException {
		UserBean user = new UserBean();
		user.firstName = name;
		user.lastName = last_name;
		user.email = email;
		user.userName = user_id;
		user.password = password;
		user.ranking = 0;
		user.role = "EndUser";
		return user;
	}
	
	/**
	 * Collection of user's properties.
	 * @return hashmap of properties
	 */
	public HashMap<String,String> getProperties() {
		HashMap<String,String> props = new HashMap<String,String>();
		props.put("first_name", this.firstName);
		props.put("last_name",this.lastName);
		props.put("user_name", this.userName);
		props.put("user_id",Integer.toString(this.userId));
		// props.put("password", this.password); hidden for security
		props.put("email", this.email);
		props.put("ranking", Float.toString(this.ranking));
		props.put("role_name", this.role);
		return props;
	}
	
}
	

