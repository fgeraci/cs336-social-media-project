package cs336.beans;

public class PsortBean {
	
	private String productName;
	
	private String price;
	
	private int productID;
	
	private int userID;
	
	private String userName;
	
	
	
	

	public PsortBean() {
		// TODO Auto-generated constructor stub
	}




	public String getProductName() {
		return productName;
	}




	public void setProductName(String productName) {
		this.productName = productName;
	}




	public String getPrice() {
		return price;
	}




	public void setPrice(String price) {
		this.price = price;
	}




	public int getProductID() {
		return productID;
	}




	public void setProductID(int productID) {
		this.productID = productID;
	}




	public int getUserID() {
		return userID;
	}




	public void setUserID(int userID) {
		this.userID = userID;
	}




	public String getUserName() {
		return userName;
	}




	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public static PsortBean createPSort(int userID, int productID, String productName,
			String price, String userName) {
			PsortBean psort = new PsortBean();
			psort.userID = userID;
			psort.userName = userName;
			psort.price = price;
			psort.productName = productName;
			psort.productID = productID;
			return psort;
		}

}
