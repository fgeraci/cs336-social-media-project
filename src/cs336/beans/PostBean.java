/**
 * 
 */
package cs336.beans;

import java.sql.Date;
import java.util.HashMap;

/**
 * Represents a Topic and its attributes.
 * @author Team 17
 *
 */

/*
 * 
 * CREATE TABLE Posts
(
	post_id int AUTO_INCREMENT,
	owner int,
	thread_id int,
	posted_date date NOT NULL,
	parent_post int,
	body varchar(5000) NOT NULL,
	votes int NOT NULL,
	rank int NOT NULL,
	PRIMARY KEY (post_id),
	FOREIGN KEY (owner) REFERENCES Users(user_id),
	FOREIGN KEY (thread_id) REFERENCES Threads(thread_id)
		ON DELETE CASCADE
) Engine=InnoDB;
 */
public class PostBean {
	private int post_owner;
	private int thread_id;
	private Date posted_date;
	private int parent_post;
	private String post_body;
	private int votes;
	private int rank;
	private HashMap<Integer, ThreadBean> threadPosts;
	private int post_id;
	/**
	 * Constructor.
	 */
	public PostBean() { 
		this.threadPosts = new HashMap<Integer,ThreadBean>(); 
	}
	public void setPostID(int postid){
		this.post_id = postid;
	}
	public void setPostOwner(int postOwner){
		this.post_owner = postOwner;
	}
	public void setThreadId(int threadID){
		this.thread_id = threadID;
	}
	public void setPostedDate(Date date){
		this.posted_date = date;
	}
	public void setParentPost(int parentPost){
		this.parent_post = parentPost;
	}
	public void setPostBody(String postBody){
		this.post_body = postBody;
	}
	public void setVotes(int vote){
		this.votes = vote;
	}
	public void setRank(int ranks){
		this.rank = ranks;
	}
	public void setThreadPost(HashMap<Integer,ThreadBean> threadPost) { 
		this.threadPosts = threadPost; 
	}
	
	
	public int getPostOwner(){
		return this.post_owner;
	}
	public int getThreadID(){
		return this.thread_id;
	}
	public Date getPostedDate(){
		return this.posted_date;
	}
	public int getParentPost(){
		return this.parent_post;
	}
	public String getPostBody(){
		return this.post_body;
	}
	public int getVotes(){
		return this.votes;
	}
	public int getRank(){
		return this.rank;
	}
	public HashMap<Integer,ThreadBean> getThreadPosts() { 
		return this.threadPosts; 
	}
	public int getPostID(){
		return this.post_id;
	}
}
