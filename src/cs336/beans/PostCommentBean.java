package cs336.beans;

import java.util.HashMap;

/*
 * CREATE TABLE PostsComments
(
	post_id int NOT NULL,
	comment varchar(3000) NOT NULL,
	comment_date date NOT NULL,
	FOREIGN KEY (post_id)
		REFERENCES Posts(post_id)
		ON DELETE CASCADE
) Engine=InnoDB;
 */
public class PostCommentBean {
	private int postComment_Id;
	private String post_Comment;
	private String post_CommentDate;
	private HashMap<Integer, PostBean> postComments;
	private int user_id;
	/**
	 * Constructor
	 */
	public PostCommentBean(){
		this.postComments = new HashMap<Integer, PostBean>();
	}
	
	public void setPostCommentID(int postCommentId){
		this.postComment_Id = postCommentId;
	}
	public void setPostComment(String postComment){
		this.post_Comment = postComment;
	}
	public void setPostCommentDate(String postCommentDate){
		this.post_CommentDate = postCommentDate;
	}
	public void setPostComment(HashMap<Integer, PostBean> postcomment){
		this.postComments = postcomment;
	}
	public void setPostCommentUser(int user){
		this.user_id = user;
	}
	
	public int getPostCommentID(){
		return this.postComment_Id;
	}
	public String getPostComment(){
		return this.post_Comment;
	}
	public String getPostCommentDate(){
		return this.post_CommentDate;
	}
	public HashMap<Integer, PostBean> getPostComments(){
		return this.postComments;
	}
	public int getPostCommentUser(){
		return this.user_id;
	}

}
