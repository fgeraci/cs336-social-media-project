package cs336.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs336.beans.UserBean;
import cs336.controller.Control;
import cs336.utilities.Logger;
import cs336.utilities.UserExceptions.UserNotFoundException;
import cs336.utilities.Utilities;

/**
 * Processes to a POST request for a login attempt.
 * @author Team 17
 *
 */

@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {
	
	protected void doGet( HttpServletRequest request, 
			HttpServletResponse response)
			throws ServletException, java.io.IOException {
		Logger.log("Validating cookie", Logger.INFO);
		try {
		Cookie[] cookies = request.getCookies();
			if(cookies.length != 0) {
				// validate cookie, redirect to main page
				Control.updateLogsCount(Integer.parseInt(Control.getCookie(cookies, "user_id").getValue()));
				response.sendRedirect("main.jsp");
			}
		} catch (Exception e) {
			response.sendRedirect("index.jsp");
		}
	}
	
	/**
	 * Processes the login request. On success, it grants access, otherwise, it redirects to failure error.
	 */
	protected void doPost(	HttpServletRequest request, 
						HttpServletResponse response)
						throws ServletException, java.io.IOException {
		try {
			
			UserBean user = new UserBean();
			user.setUserName(request.getParameter("user_name"));
			user.setPassword(request.getParameter("password"));
			try {
				// set the new cookie up
				int userID = Control.authenticateUser(user);
				// user.setUserId(id);
				Control.initializeCookies(user, response);
				Logger.log("Cookie just created for user "+user.getUserId(), Logger.SUCCESS);
				Control.updateLogsCount(userID);
				response.sendRedirect("main.jsp");
				// REDIRECT TO LOGIN PAGE
			}
			catch (UserNotFoundException e) {
				System.out.println(e);
				response.sendRedirect("index.jsp?error");
				// REDIRECT TO FAILED LOGIN
			}
			
		} catch (Exception e) { 
			// FATAL, this should not happen
			Logger.log(e.getMessage(), Logger.ERROR);
			response.sendRedirect("error.jsp");
		}
		
	}
}
