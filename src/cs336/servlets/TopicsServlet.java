package cs336.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TopicsServlet extends HttpServlet {
	
	public void doPost( HttpServletRequest request, 
						HttpServletResponse response)
								throws ServletException, java.io.IOException {
		response.sendRedirect("thread.jsp?t=1");
	}
}
