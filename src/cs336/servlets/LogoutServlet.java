package cs336.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs336.utilities.Logger;

@SuppressWarnings("serial")
public class LogoutServlet extends HttpServlet {
	
	protected void doGet ( 	HttpServletRequest request,
							HttpServletResponse response) 
							throws ServletException, java.io.IOException {
		Cookie[] cookies = request.getCookies();
		String name = "";
		for (Cookie c : cookies) {
			if(c.getName().equalsIgnoreCase("user_name")) name = c.getValue();
			Cookie c2 = new Cookie(c.getName(),c.getValue());
			c2.setMaxAge(0); // clear all cookies
			response.addCookie(c2);
		}
		Logger.log("User "+name+" logged out, all cookies cleared", Logger.INFO);
		response.sendRedirect("index.jsp");
	}
}
