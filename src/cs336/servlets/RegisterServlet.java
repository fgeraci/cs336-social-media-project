package cs336.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs336.beans.UserBean;
import cs336.controller.Control;
import cs336.utilities.Logger;

/**
 * 
 * Processes a registration request. On success, it responds with a positive redirect. Otherwise, it redirects to a registration failure state.
 * @author Team 17
 *
 */

@SuppressWarnings("serial")
public class RegisterServlet extends HttpServlet {
	
	/**
	 * Attempts to register a user upon end-user's request on the Database.
	 * @throws ServletException, IOException
	 */
	public void doPost( HttpServletRequest request, 
						HttpServletResponse response)
								throws ServletException, java.io.IOException {
		String name = request.getParameter("first_name");
		String lastName = request.getParameter("last_name");
		String email = request.getParameter("email");
		String user_id = request.getParameter("user_id");
		String password = request.getParameter("password");
		String passwordConf = request.getParameter("confpassword");
		if(!password.equals(passwordConf)) {
			response.sendRedirect("index.jsp?error3");
			Logger.log("password mismatch on register", Logger.ERROR);
		} else {
			try {
				UserBean user = UserBean.createRegistrationUser(name, lastName, email, user_id.toLowerCase(), password);
				int val = Control.registerUser(user);
				Logger.log("User successfully registered: "+user_id, Logger.SUCCESS);
				Control.initializeCookies(user, response);
				
				/*Cookie cookieId = new Cookie("user_id", Integer.toString(val));
				Cookie cookieName = new Cookie("user_id", name);
				cookieId.setMaxAge(120); // 30 mins sessions if inactive
				cookieName.setMaxAge(120);
				response.addCookie(cookieId);
				response.addCookie(cookieName);*/
				
				response.sendRedirect("reg.jsp");
			} catch (Exception e) { 
				Logger.log(e.getMessage(), Logger.ERROR);
				response.sendRedirect("index.jsp?error2");
			}
		}
	}
		
					

}
