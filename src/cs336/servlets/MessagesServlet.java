package cs336.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs336.controller.Control;
import cs336.utilities.Logger;
import cs336.utilities.UserExceptions.UserNotFoundException;

/**
 * Manages all message traffic and requested actions.
 * @author Team 17
 *
 */

@SuppressWarnings("serial")
public class MessagesServlet extends HttpServlet {
	
	protected void doPost( HttpServletRequest request, 
			HttpServletResponse response)
			throws ServletException, java.io.IOException {
		Logger.log("Processing message", Logger.INFO);
		String url = "messages.jsp?c=1";
		String from = "";
		String to= "";
		try {
			if(request.getParameter("openMessage") != null) {
				int messageID = Integer.parseInt(request.getParameter("messageID"));
				url = "messages.jsp?o=1"+"&m="+messageID;
					Control.markAsRead(messageID);
			} else if (request.getParameter("deleteMessage") != null) {
				int messageID = Integer.parseInt(request.getParameter("messageID"));
				Control.deleteMessage(messageID);
			} else if (request.getParameter("sendMessage") != null) {
				to = request.getParameter("destination");
				from = Control.getCookie(request.getCookies(), "user_name").getValue();
				String subject = request.getParameter("subject");
				String body = request.getParameter("content");
				body = body.replaceAll("\r\n|\n", "<br />");
				int toId = Control.getUserIdFromName(to);
				if(toId == -1) throw new UserNotFoundException();
				int fromId = Integer.parseInt(Control.getCookie(request.getCookies(), "user_id").getValue());
				Control.sendMessage(fromId, toId, subject, body);
			} else if (request.getParameter("search") != null) {
				Logger.log("Executed messages search", Logger.INFO);
				String searchFor = request.getParameter("searchString").replaceAll(" ", "%");
				url = "messages.jsp?c=1&search="+request.getParameter("searchString");
			} else if (request.getParameter("deleteSentMessage") != null) {
				int messageID = Integer.parseInt(request.getParameter("messageID"));
				Control.deleteMessage(messageID);
				url = "sent_messages.jsp?c=1";
			}
		} catch (Exception e) { 
			url="messages.jsp?c=1&e=1";
			Logger.log("User: "+from+" failed to send message to: "+to,Logger.ERROR);
		}
		response.sendRedirect(url);
	}

 }
