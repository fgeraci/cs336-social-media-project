package cs336.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs336.beans.ThreadBean;
import cs336.beans.UserBean;
import cs336.beans.ProductBean;
import cs336.controller.Control;
import cs336.utilities.Logger;
import cs336.utilities.UserExceptions.UserFieldsErrorException;
import cs336.utilities.UserExceptions.UserNotFoundException;
import cs336.utilities.Utilities;

/**
 * 
 * Will redirect the managers flow as needed based upon permissions.
 * @author Team 17
 *
 */

@SuppressWarnings("serial")
public class ManagerServlet extends HttpServlet {
	
	protected void doGet( HttpServletRequest request, 
			HttpServletResponse response)
			throws ServletException, java.io.IOException {
		Logger.log("Checking manager permissions", Logger.INFO);
		Cookie[] cookies = request.getCookies();
		String val = null;
		for(Cookie c : cookies) {
			if(c.getValue().equalsIgnoreCase("Administrators")) {
				val = c.getValue();
			} else if(c.getValue().equalsIgnoreCase("Sales")) {
				// redirect to sales directly
			}
		}
		if (val == null) { // triple security check
			response.sendRedirect("index.jsp");
		} else {
			Logger.log("Administrator selection screen", Logger.INFO);
			response.sendRedirect("manager.jsp");
		}
	}
	
	protected void doPost(	HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		boolean error = false;
		int value = 0;
		String sortBy="";
		String tSortBy="";
		int parentPost = 0;
		int userID1 = 0;
		// the most stupid and cheapest flag I have ever used... but we need to stick to Java 6, hence no switch on strings.
		int event = 2; // 1 - main 2 - manager
		try {
		 // here is the userID, somewhat obscured
	 		if(request.getParameter("modify") != null) {
	 			value = Integer.parseInt(request.getParameter("modify"));
	 			String password = request.getParameter("modPassword");
	 			String passwordConf = request.getParameter("modPasswordConf");
	 			String role = request.getParameter("modRoleName");
	 			if(password != null) {
		 			if(password.length() != 0 && password.equals(passwordConf)) {
		 				Control.updateUserPassword(value, password);
		 			}
	 			}
	 			Control.updateRole(value, Utilities.getRoleID(role));
				Logger.log("UserID "+value+" modify", Logger.INFO);
			} else if(request.getParameter("delete") != null) {
				value = Integer.parseInt(request.getParameter("delete"));
				Control.deleteUser(value);
				Logger.log("UserID "+value+" delete", Logger.INFO);	
			/*Also added by Harris*/	
			}else if(request.getParameter("deleteProd") != null){ 
				value = Integer.parseInt(request.getParameter("deleteProd"));
				Control.deleteProd(value);
				Logger.log("ProductID" +value+" delete", Logger.INFO);
				event = 7;
			}else if (request.getParameter("addUser") != null) {
				addUser(request,response);
				Logger.log("Attempting to create user", Logger.INFO);
			/*Added by Harris, coolest kid in school	*/
			}else if (request.getParameter("addProduct") != null) { 
				addProduct(request,response);
				Logger.log("Attempting to create product", Logger.INFO);
				event = 7;
			}else if (request.getParameter("createTopic") != null) {
				String newTopicName = request.getParameter("newTopicName");
				Control.addNewTopic(newTopicName);
				Logger.log("New topic created: "+newTopicName, Logger.INFO);
				event = 1;
			} else if (request.getParameter("deleteTopic") != null) {
				String topic = request.getParameter("deleteTopic");
				Control.deleteTopic(Integer.parseInt(topic));
				Logger.log("Topic deleted: "+topic, Logger.INFO);
				event = 1;
			} else if (request.getParameter("modifyTopicName") != null) {
				int id = Integer.parseInt(request.getParameter("modifyTopicName"));
				Control.renameTopic(id, request.getParameter("newTopicName"));
				event = 1;
				Logger.log("Topic name change requested for: "+ id, Logger.INFO);	
			} else if(request.getParameter("createThread") != null){
				String newThreadName = request.getParameter("newThreadName");
				int userID = Integer.parseInt(Control.getCookie(request.getCookies(), "user_id").getValue());
				Control.addNewThread(newThreadName, Integer.parseInt(request.getParameter("topicID")), userID);
				Logger.log("New thread created: " + newThreadName, Logger.INFO);
				event = 5;
				
			}
			else if (request.getParameter("deleteThread") != null) {
				String thread = request.getParameter("deleteThread");
				Control.deleteThread(Integer.parseInt(thread));
				Logger.log("Thread deleted: " + thread, Logger.INFO);
				event = 5;
			} else if (request.getParameter("modifyThreadName") != null) {
				int id = Integer.parseInt(request.getParameter("modifyThreadName"));
				Control.renameThread(id, request.getParameter("newThreadName"));
				event = 5;
				Logger.log("Thread name change requested for: " + id, Logger.INFO);	
			} else if(request.getParameter("modifyComment") != null){ 
				int commentID = Integer.parseInt(request.getParameter("modifyComment"));
				String comment = request.getParameter("newComment");
				Control.editComment(commentID, comment);
				Logger.log("Comment modified: " + commentID, Logger.INFO);
				event = 6;
			}
			
			else if (request.getParameter("createPost") != null){
				String newPost = request.getParameter("content");
				int userID = Integer.parseInt(Control.getCookie(request.getCookies(), "user_id").getValue());
				Control.addNewPost(newPost, 0, userID, Integer.parseInt(request.getParameter("threadID")));		
				event = 6;
			} else if(request.getParameter("deletePost") != null){
				int postID = Integer.parseInt(request.getParameter("deletePost"));
				Control.deletePost(postID);
				Logger.log("Post deleted: " + postID, Logger.INFO);
				event = 6;
			} else if(request.getParameter("deleteComment") != null){ 
				int commentID = Integer.parseInt(request.getParameter("deleteComment"));
				Control.deleteComment(commentID);
				Logger.log("Comment deleted: " + commentID, Logger.INFO);
				event = 6;
			}
			
			else if(request.getParameter("modifyPost") != null){
				int postID = Integer.parseInt(request.getParameter("modifyPost"));
				String postBody = request.getParameter("newPost");
				System.out.println("Revised Post Body: " + postBody);
				Control.modifyPost(postID, postBody);
				Logger.log("Post modified: " + postID, Logger.INFO);
				event = 6;
			}
			else if (request.getParameter("sortTopics") != null) {
				Logger.log("sort of topics requested", Logger.INFO);
				sortBy = request.getParameter("sortTopicsValue");
				event = 3;
			} else if (request.getParameter("search") != null) {
				event = 4;
			} else if (request.getParameter("openTopic") != null) {
				event = 5;
			} else if (request.getParameter("openThread") != null){
				event = 6;
			} else if(request.getParameter("voteUpThread") != null){
				Logger.log("Vote up for Thread: " + request.getParameter("voteUpThread"), Logger.INFO);
				Control.updateThreadVoteCount(Integer.parseInt(request.getParameter("voteUpThread")));
				event = 5;
			} else if(request.getParameter("voteDownThread") != null){ 
				Logger.log("Vote down for Thread: " + request.getParameter("voteDownThread"), Logger.INFO);
				Control.decreaseThreadVoteCount(Integer.parseInt(request.getParameter("voteDownThread")));
				event = 5;
			} else if(request.getParameter("voteUpPost") != null){
				int postID = Integer.parseInt(request.getParameter("voteUpPost"));
				Logger.log("Vote up for Post: " + postID, Logger.INFO);
				Control.updatePostVoteCount(postID);
				event = 6;
			} else if(request.getParameter("voteDownPost") != null){
				int postID = Integer.parseInt(request.getParameter("voteDownPost"));
				Logger.log("Vote down for Post: " + postID, Logger.INFO);
				Control.decreasePostVoteCount(postID);
				event = 6;
			}
			else if(request.getParameter("createComment") != null){
				String commentBody = request.getParameter("newCommentContent");
				System.out.println("commentBody: " + commentBody);
				parentPost = Integer.parseInt(request.getParameter("createComment"));
				System.out.println("parentPost: " + parentPost);
				
				userID1 = Integer.parseInt(Control.getCookie(request.getCookies(), "user_id").getValue());
				System.out.println("userid: " + userID1);
				Control.addComment(parentPost, commentBody, userID1);
				event = 6;
			}
			
			else {
				throw new Exception("Error handling topic");
			}
		} catch (Exception ex) {
			Logger.log(ex.getMessage(), Logger.ERROR);
			error = true;
			event = -1;
			Logger.log("POST processing of administration error. " + ex.getMessage(),Logger.ERROR);
			
		} finally {
			String url = "main.jsp";
			switch(event) {
			case 1:
				url = "main.jsp";
				break;
			case 2:
				url = "manager.jsp";
				break;
			case 3:
				if(sortBy.equalsIgnoreCase("alphabetically")) {
					sortBy = "?sb=1";
				} else if (sortBy.equalsIgnoreCase("OverallVotes")) {
					sortBy = "?sb=2";
				} else if (sortBy.equalsIgnoreCase("NumPosts")) {
					sortBy = "?sb=3";
				} else if (sortBy.equalsIgnoreCase("dataPublished")) {
					sortBy = "?sb=4";
				} else if (sortBy.length() < 1) {
					sortBy = "";
				}
				url = "main.jsp"+sortBy;
				break;
			case 4:
				url = "manager.jsp?search="+request.getParameter("searchString");
				break;
			/*Modified by Harris*/	
			case 5:
				url = "thread.jsp?t="+request.getParameter("topicID");
				String topicName = request.getParameter("topicName");
				String userKeywords = null;
				String insert = null;
				int userID = Integer.parseInt(Control.getCookie(request.getCookies(), "user_id").getValue());
				try {
					userKeywords = Control.getCustomerKeywords(userID);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if( userKeywords == null){
					insert = topicName;
				}
				else{
					insert = userKeywords + "," + topicName;
				}
				try {
					Control.updateUserKeywords(userID,insert);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case 6:
				url = "threadposts.jsp?threadID=" + request.getParameter("threadID") + "&topicID=" + request.getParameter("topicID");
				break;
			case 7:	
				url = "sales.jsp";
				break;
			default:
				url = error ? "main.jsp?error" : "main.jsp";
			}
			response.sendRedirect(url);
		}
	}
	
	private void addUser(HttpServletRequest request, HttpServletResponse response) 
			throws IOException, UserFieldsErrorException, IllegalAccessException, InstantiationException, ClassNotFoundException, UserNotFoundException, SQLException {
		String name = request.getParameter("first_name");
		String lastName = request.getParameter("last_name");
		String email = request.getParameter("email");
		String user_id = request.getParameter("user_id");
		String password = request.getParameter("password");
		String passwordConf = request.getParameter("confpassword");
		String role = request.getParameter("role_name");
		if(!password.equals(passwordConf)) {
			Logger.log("password mismatch on register", Logger.ERROR);
			throw new UserFieldsErrorException();
		} else {
			UserBean user = UserBean.createRegistrationUser(name, lastName, email, user_id.toLowerCase(), password);
			int newUserID = Control.registerUser(user);
			Control.updateRole(newUserID, Utilities.getRoleID(role));
			Logger.log("User successfully registered: "+user_id, Logger.SUCCESS);
		}
	}
	//Added by Harris. Adding product to the database.
	private void addProduct(HttpServletRequest request, HttpServletResponse response)
			throws IOException, UserFieldsErrorException, IllegalAccessException, InstantiationException, ClassNotFoundException, UserNotFoundException, SQLException {
		int productID = 0;
		String manufacturer = request.getParameter("manufacturer");
		String picture = request.getParameter("picture");
		String keywords = request.getParameter("keywords");
		String price = request.getParameter("price");
		ProductBean product = ProductBean.createRegistrationProduct(productID, manufacturer, picture, keywords, price);
		int newProductID = Control.registerProduct(product);
		Logger.log("Product successfully registered: "+newProductID, Logger.SUCCESS);
	}
}
