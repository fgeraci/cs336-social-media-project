<%@page import="cs336.controller.Control"%>
<%@ page import="cs336.utilities.Logger" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="styles/reg_theme.css" rel="stylesheet" />
<link href="styles/main_theme.css" rel="stylesheet"/>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TRH - Registration completed</title>
</head>
<body>
<%
	/**
	* Validate the existence of a cookie for this access.
	*/
	boolean passed = Control.validateCookie(request);
	if(!passed) {
		Logger.log("Unauthorized access to reg.jsp", Logger.ERROR);
		response.sendRedirect("index.jsp");
	} else {
		Logger.log("Cookie existence validated", Logger.SUCCESS);
	}
%>
<div id="welcomeDIV" >
	<p id="successMessage">REGISTRATION SUCCESSFUL</p>
	<br>
	<p> welcome to the pack<p><br>
	<!-- Hard validation through Servlet is not required because the user is either legitimate or cracking, both cases are handled -->
	<a id="loginButton" href="main.jsp"><img style="margin-top: 25px;" src="images/clogin_icon.png" alt="Login"></a>
</div>
</body>
</html>