<%@page import="cs336.utilities.Logger"%>
<%@page import="cs336.controller.Control"%>
<%@page import="cs336.beans.UserBean" %>
<%@page import="cs336.beans.ProductBean" %>
<%@page import="cs336.beans.PsdBean" %>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
	errorPage="error.jsp"
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" href="images/cake_icon.png" />
<title>Harris' Addiction</title>
<link href="styles/main_theme.css" rel="stylesheet">
<link href="styles/logged_style.css" rel="stylesheet">
<link href="styles/menu.css" rel="stylesheet">
<link href="styles/manager_style.css" rel="stylesheet">
<link href="styles/table_theme.css" rel="stylesheet">
<script type="text/javascript" src="scripts/utilities.js"></script>
<script type="text/javascript" src="scripts/logged.js"></script>
<script type="text/javascript" src="scripts/manager.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manager Selection</title>

</head>
<body>
	<%
		// secure access
		if(!(Control.validateCookie(request))) response.sendRedirect("index.jsp");
	%>
	<div class="LoggedHeader">
		<div id="logoDIV">
			<img id="miniLogo" src="images/mini_logo.png" alt="mini logo">
		</div>
		<div class="Options">
			<a href="main.jsp">
			<img class="Icons" id="homeIcon" src="images/home_icon.png" alt="home">
			</a>
			<a href="messages.jsp?c=1">
			<img class="Icons" id="messagesIcon" src="images/messages_icon.png" alt="messages">
			</a>
			<img class="Icons" id="gearIcon" src="images/gear.png" alt="settings">
			<div class="PopUp" id="menuDIV">
				<img src="images/x_orange.png" alt="close" class="Icons" id="closeMenuDIV">
				<div style='margin-top: 10px; text-align: center;'>
					<img style='margin: 10px;' class="Icons" src="images/profile_icon.png" alt="profile">
					<a href=" "><img class="Icons" src="images/sales_icon_text.png" alt="ads"></a>
					<a href="Logout"><img style='margin: 10px;' class="Icons" src="images/logout_icon.png" alt="logout"></a>
				</div>
			</div>
		</div>
	</div> 
	<div id="usersDIV">
		<table id="usersTable">
			<tr>
				<th>ID</th>
				<th>Name</th>
			 	<th>Product</th>
			  	<th>Amount Paid</th>
			</tr>
			<%
			try {
				ArrayList<PsdBean> psds = null;
				if(request.getParameter("search") != null) {
					/*products = Control.trimUsers(request.getParameter("search"));*/
				} else {
					psds = Control.retrieveAllPSD();
				}
				int x = 0;
				for(PsdBean psd : psds) {
					%>
					<%if(x != psd.getUserID()){
						x = psd.getUserID();
					%>						
					<tr> 
						<td><%= psd.getUserID() %></td>
						<td> <%=psd.getName()%> </td>
					</tr>
						
						
					<% 					}%>
					<tr>
						<td></td>
						<td></td>
						<td><%= psd.getProductName() %> </td>
						<td><%= psd.getPaid()%> </td>
					</tr>
					<%
				}
			} catch (Exception e) {
				Logger.log("Product table error", Logger.ERROR);
				// something went really wrong with the DB
			}
			%>
			</table>
	</div>	
	<%@ include file="advert.jsp" %>
</body>
</html>