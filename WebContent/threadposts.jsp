<%@page import="cs336.utilities.Utilities"%>
<%@page import="cs336.utilities.Logger"%>
<%@page import="cs336.controller.Control"%>
<%@page import="java.util.ArrayList" %>
<%@page import="cs336.beans.TopicBean" %>
<%@page import="cs336.beans.ThreadBean" %>
<%@page import="cs336.beans.PostBean" %>
<%@page import="cs336.beans.PostCommentBean" %>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    errorPage="error.jsp"
    %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" href="images/cake_icon.png" />
<title>Harris' Addiction</title>
<link href="styles/main_theme.css" rel="stylesheet">
<link href="styles/logged_style.css" rel="stylesheet">
<link href="styles/menu.css" rel="stylesheet">
<script type="text/javascript" src="scripts/utilities.js"></script>
<script type="text/javascript" src="scripts/logged.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<body>
	<div class="LoggedHeader">
		<div id="logoDIV">
			<img id="miniLogo" src="images/mini_logo.png" alt="mini logo">
		</div>
		<div class="Options">
			<span id="searchSPAN">
				<input type="text" name="searchField" placeholder="search">
				<a href="call the search servlet"><img class="Icons" src="images/search_icon.png" alt="search"></a>
			</span>
			<a href="messages.jsp?c=1">
			<%
			try {
				Cookie c = null;
				c = Control.getCookie(request.getCookies(), "user_id");
			if(c != null) {
				if(Control.hasUnreadMessages(Integer.parseInt(c.getValue()))) {
			%>
						<img class="Icons" id="messagesIcon" src="images/new_message_icon.png" alt="messages">
			<%
					} else {
			%>
						<img class="Icons" id="messagesIcon" src="images/messages_icon.png" alt="messages">
			<%
					}
				} 
			} catch( Exception e) {
				response.sendRedirect("index.jsp");
			}
			%>
			
			</a>
			<img class="Icons" id="gearIcon" src="images/gear.png" alt="settings">
			<div class="PopUp" id="menuDIV">
				<img src="images/x_orange.png" alt="close" class="Icons" id="closeMenuDIV">
				<div style='margin-top: 10px; text-align: center;'>
					<img class="Icons" src="images/profile_icon.png" alt="profile">
					<a href="Logout"><img class="Icons" src="images/logout_icon.png" alt="logout"></a>
					<%
					String value = null;
					boolean admin = false;
					boolean sales = false;
					boolean moderator = false;
					String username = "";
					try {
						value = Control.getCookie(request.getCookies(), "role_name").getValue();
						sales = Control.isSales(value);
						admin = Control.isManager(value);
						moderator = Control.isModerator(value);
						username = Control.getCookie(request.getCookies(), "user_name").getValue();
						if(admin || sales) {
							Logger.log("Administrator just logged", Logger.INFO);
							%><a href=" "><img class="Icons" src="images/sales_icon_text.png" alt="ads"></a><%
							if(admin) {
								%><a href="Manager"><img class="Icons" src="images/users_icon_text.png" alt="admin"></a><%
							}
						}
					} catch (Exception e) { 
						Logger.log("No cookies available, only acceptable if testing  "+e.getMessage(), Logger.ERROR);
						// response.sendRedirect("index.jsp");
					}
					%>
				</div>
			</div>
		</div>
	</div> 										
		<% 
		int threadID = Integer.parseInt(request.getParameter("threadID"));
		int topicID = Integer.parseInt(request.getParameter("topicID"));
		String threadName = Control.retrieveThread(threadID, topicID).getThreadName().toUpperCase();
		%>
		
		<div id="addPostDIV" class="PopUp">
			<form action="Manager" method="POST">
				<div style="width: 95%; margin-bottom: 15px; margin-left: 0; margin-top: 10px; text-align: right; padding-left: 10px;">
					<img src="images/x_orange.png" alt="close" class="Icons" onclick="hideAddPost()">
				</div>
				<div id="newPostParagraphDIV" style="text-align: center;">
					<input name="threadID" type="hidden" value="<%= threadID %>">
					<input name="topicID" type="hidden" value="<%= topicID %>">
					<textarea id="textArea" name="content"></textarea>
				</div>
				<div style="width: 95%; margin-top: 35px; margin-bottom: 20px; margin-left: 10px; margin-top: 10px; text-align: left; padding-left: 10px;">
					<input name="createPost" type="submit" value="Post" class="Icons">
				</div>
			</form>
		</div>
		<div class="PageBody">
			<div class="PostDisplay" style="display:inline-block;">
				<span style="vertical-align: middle;">	
					<h1><%= threadName %></h1>	
				</span>				
			</div>	
			<img class="Icons" id="addPost" src="images/add_user_icon.png" alt="Edit" onclick="showAddPost();">
			<br>
			<br>
			<% 
			ArrayList<PostBean> posts = Control.getAllPostsFromThread(threadID);
			int parentPostID = 0;
			if(posts != null){
				for(int k = 0; k < posts.size(); k++){
					String userName = Control.getUserNameFromId(posts.get(k).getPostOwner());
					String postDate = posts.get(k).getPostedDate().toString();
					parentPostID = posts.get(k).getPostID();
					%>
					<form id="postForm" method="POST" action="Manager">
						<input type="submit" name="voteUpPost" value="<%= parentPostID %>" class="thumbsupIcon Icons"> 
						<input type="submit" name="voteDownPost" value="<%= parentPostID %>" class="thumbsdownIcon Icons">
						<input type="submit" id="addComment" name = "addComment" value="<%= parentPostID %>" class="addIcon Icons" onclick="return showAddComment(this);"> 
						<h1><%= Control.getOverallPostVotes(parentPostID) %></h1>
						<div id="postDIV" style="text-align: center;">
							<div id="postParagraph">
								<%= userName %> : <%= postDate %> : <%= parentPostID %>
								<br> <br>
								<p id="postParagraph">
									<%= posts.get(k).getPostBody()	%>
								</p>
								<div class="Icons" style="display:inline-block;">
									<input name="threadID" type="hidden" value="<%= request.getParameter("threadID") %>">
									<input name="topicID" type="hidden" value="<%= request.getParameter("topicID") %>">
									<%
									if(userName.equalsIgnoreCase(username) || admin || moderator){ %>
										<input type="submit" name="deletePost" value="<%= parentPostID %>" class="deleteIcon Icons">
										<input type="image" src="images/pencil_icon.png" name="modifyPost" value="<%= parentPostID %>" class="modifyIcon Icons" onclick="return showModPost(this)">
									<%}
									
									%>
								</div>
							</div>
						</div>		
					</form>									
					
					<%
					ArrayList<PostCommentBean> postComments = Control.getAllCommentsFromPost(parentPostID);
						for(int i = 0; i < postComments.size(); i++){
							String commentuserName = Control.getUserNameFromId(postComments.get(i).getPostCommentUser());
							String commentpostDate = postComments.get(i).getPostCommentDate().toString();
					%>
						<form id="commentForm" method="POST" action="Manager">	
							<div id="commentDIV" style="text-align: center;">
								<div id="commentParagraph">
									<%= commentuserName %> : <%= commentpostDate %> : <%= parentPostID %>
									<br> <br>
									<p id="commentParagraph">
										<%= postComments.get(i).getPostComment() %>
									</p>
									<div class="Icons" style="display:inline-block;">
										<input name="threadID" type="hidden" value="<%= request.getParameter("threadID") %>">
										<input name="topicID" type="hidden" value="<%= request.getParameter("topicID") %>">
										<%
										if(commentuserName.equals(username) || admin || moderator){ %>
											<input type="submit" name="deleteComment" value="<%= postComments.get(i).getPostCommentID() %>" class="deleteIcon Icons">
											<input type="image" src="images/pencil_icon.png" name="modifyComment" value="<%= postComments.get(i).getPostCommentID() %>" class="modifyIcon Icons" onclick="return showModComment(this)">
										<%}
										
										%>
									</div> 
								</div>
							</div>	
							<br>	
						</form>
						<%}
					 %>
					<%
					}
				} %>

		<div id="modPostDIV" class="PopUp" style='text-align: right;'>
			<img src="images/x_orange.png" alt="close" class="Icons" onclick="hideModPost()">
				<div style='text-align: center;'>
					<input form="postForm" type="text" name="newPost"> 
					<input form="postForm" style="width: auto;color:rgba(0,0,0,0); background: url('images/check_icon.png') no-repeat; box-shadow: none; margin-right: 15px;" 
						id="newPostID" name="modifyPost" type="submit" class="Icons" value="Modify Post" > 
				</div> 
		</div>
		<div id="modCommentDIV" class="PopUp" style='text-align: right;'>
			<img src="images/x_orange.png" alt="close" class="Icons" onclick="hideModComment()">
				<div style='text-align: center;'>
					<input form="postForm" type="text" name="newComment" placeholder="New Comment..." > 
					<input form="postForm" style="width: auto;color:rgba(0,0,0,0); background: url('images/check_icon.png') no-repeat; box-shadow: none; margin-right: 15px;" 
						id="newCommentID" name="modifyComment" type="submit" class="Icons" value="Modify Comment" > 
				</div> 
		</div>
		
		<div id="addCommentDIV" class="PopUp">
			<form action="Manager" method="POST" style="margin-right: 15px;">
				<div style="margin: auto; width:100%;text-align: left;">
					<img src="images/x_orange.png" alt="close" class="Icons" onclick="hideAddComment()">
				</div>
				<div id="newCommentParagraphDIV" style="text-align: center;">
					<input name="threadID" type="hidden" value="<%= threadID %>">
					<input id = "commentparentPostID" name="parentPostID" type="hidden"> 
					<input name="topicID" type="hidden" value="<%= topicID %>">
					<textarea id="textArea" name="newCommentContent"></textarea>
				</div>
				<div style="width: 95%; margin-top: 35px; margin-bottom: 20px; margin-left: 10px; margin-top: 10px; text-align: left; padding-left: 10px;">
					<input class="Icons" id="postIDHolder" name="createComment" type="submit" value="Post Comment" class="Icons">
				</div>
			</form>
		</div>
	</div> 
</body>
</html>