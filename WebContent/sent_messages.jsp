<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    errorPage="error.jsp"
    %>
 <%@page import="cs336.controller.*" %>
 <%@page import="cs336.utilities.*" %>
 <%@page import="java.util.*" %>
 <%@page import="cs336.beans.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>HBA My Messages</title>
<link rel="shortcut icon" href="images/cake_icon.png" />
<link href="styles/main_theme.css" rel="stylesheet">
<link href="styles/menu.css" rel="stylesheet">
<link href="styles/messages_style.css" rel="stylesheet">
<script type="text/javascript" src="scripts/utilities.js"></script>
<script type="text/javascript" src="scripts/logged.js"></script>
<script type="text/javascript" src="scripts/messages.js"></script>

</head>
<body>
	<div class="LoggedHeader">
		<div id="logoDIV">
			<img id="miniLogo" src="images/mini_logo.png" alt="mini logo">
		</div>
		<div class="Options">
			<a href="main.jsp">
				<img class="Icons" id="homeIcon" src="images/home_icon.png" alt="home">
			</a>
			<img class="Icons" id="gearIcon" src="images/gear.png" alt="settings">
			<div class="PopUp" id="menuDIV">
				<img src="images/x_orange.png" alt="close" class="Icons" id="closeMenuDIV">
				<div style='margin-top: 10px; text-align: center;'>
					<img class="Icons" src="images/profile_icon.png" alt="profile">
					<a href="Logout"><img class="Icons" src="images/logout_icon.png" alt="logout"></a>
					<%!
						int currentMessage = -1;
						HashMap<Integer,String> messagesBody = new HashMap<Integer,String>();
					%>
					<% 
					String value = null;
					boolean admin = false;
					boolean sales = false;
					boolean moderator = false;
					try {
						value = Control.getCookie(request.getCookies(), "role_name").getValue();
						sales = Control.isSales(value);
						admin = Control.isManager(value);
						moderator = Control.isModerator(value);
						if(admin || sales) {
							Logger.log("Administrator just logged", Logger.INFO);
							%><a href=" "><img class="Icons" src="images/sales_icon_text.png" alt="ads"></a><%
							if(admin) {
								%><a href="Manager"><img class="Icons" src="images/users_icon_text.png" alt="admin"></a><%
							}
						}
					} catch (Exception e) {
						Logger.log("No cookies available, only acceptable if testing  "+e.getMessage(), Logger.ERROR);
					}
					%>
				</div>
			</div>
		</div>
	</div> 
	<div id="contentBody" style="float: left;">
		<div id="searchBar" class="InnerDivs">
			<form action="Messages" method="POST">
				<input type="search" placeholder="search message..." name="searchString">
				<input style="box-shadow: none; margin-left: 10px; background: url('images/mini_search_icon.png') no-repeat; color: rgba(0,0,0,0); width: 21px;" name="search" type="submit" class="Icons">
			</form>
		</div>
		<span id="messagesBody">
			<div id="actionsDIV" class="InnerDivs">	
				<ul style="list-style: none;">
					<li class="Icons" id="newMessageLi" onclick="openNewMessage(this)">New Message</li>
					<a style="color: inherit; text-decoration: none;" href="messages.jsp?c=1">
						<li class="Icons">Inbox</li>
					</a>
					<a style="color: inherit; text-decoration: none;" href="sent_messages.jsp?c=1">
						<li class="Icons">Sent</li>
					</a>
					<!-- <a style="color: inherit; text-decoration: none;" href="messages.jsp?c=3">
						<li class="Icons">Trash</li>
					</a> -->
				</ul>
			</div>
			<div id="messagesDIV" class="InnerDivs">
				<table style="width: 100%" id="messagesTable">
					<tr>
						<th style="border-radius: 5px 0 0 0;"></th>
						<th >date</th>
						<th>to</th>
						<th style="border-radius: 0 5px 0 0;">subject</th>
					</tr>
					<%
					int sectionID = 1;
					try {
						sectionID = Integer.parseInt(request.getParameter("c"));
					} catch (Exception e) { }
					int userID = -1;
					userID = Integer.parseInt(Control.getCookie(request.getCookies(), "user_id").getValue());
					// implement search result here.
					ArrayList<MessageBean> messages = null;
					if(request.getParameter("search") != null) {
						messages = Control.searchMessages(userID, request.getParameter("search"));
						
					} else {
						messages = Control.getMessages(userID);
					}
					
					switch(sectionID) {
					case 1: // inbox
						for(MessageBean m : messages) {
							messagesBody.put(m.getMessageID(), m.getBody());
							if(m.getOwnerID() == userID) {								
								%>
								<form action="Messages" method="POST">
								<tr class="MessageRow">
									<td id="iconsHolderTd">
										<div>
										<input type="hidden" value="<%=m.getMessageID() %>" name="messageID" name="deleteMessage">
										<input name="deleteSentMessage" class="Icons IconInputs" id="deleteMessageInput" type="submit">
										<img name="openSentMessage" style="vertical-align: middle;" class="Icons IconInputs" id="openMessageInput" onclick="return openMessage(<%= m.getMessageID()%>,0,'<%= m.getBody()%>',this)">
										</div> 
									</td>
									<td><%= m.getDate() %></td>
									<% 
										String userName = Control.getUserNameFromId(m.getReceiverID());
									%>
									<td><%= userName %></td>
									<td><%=m.getSubject() %></td>
								</tr>
								</form>
								<%
							}
						}
						break;
					case 2: // sent
						break;
					case 3: // trashed
						break;
					}
					%>
				</table>
			</div>
		</span>	
	</div>
	<div id="messageBodyDIV" class="PopUp">
		<div style="width: 95%; margin-bottom: 15px; margin-left: 0; margin-top: 10px; text-align: right; padding-left: 10px;">
			<img src="images/x_orange.png" alt="close" class="Icons" onclick="closeMessage(this)">
		</div>
		<div id="messageDIV" style="text-align: center;">
			<div id="messageParagraph">
				<p id="messageParagraph">
					<%= messagesBody.get(currentMessage) %>
				</p>
			</div>
		</div>
	</div>
	<div id="newMessageDIV" class="PopUp">
		<form action="Messages" method="POST">
		<div style="width: 95%; margin-bottom: 15px; margin-left: 0; margin-top: 10px; text-align: right; padding-left: 10px;">
			<img src="images/x_orange.png" alt="close" class="Icons" onclick="closeNewMessage()">
		</div>
		<div style="width: 95%; margin-bottom: 5px; margin-left: 10px; margin-top: 10px; text-align: left; padding-left: 10px;">
			<input name="destination" type="text" placeholder="To...">
			<br>
			<input name="subject" style="margin-top: 10px; width: 95%" type="text" placeholder="Subject...">
		</div>
		<div id="newMessageParagraphDIV" style="text-align: center;">
				<textarea id="textArea" name="content"></textarea>
		</div>
		<div style="width: 95%; margin-top: 35px; margin-bottom: 20px; margin-left: 10px; margin-top: 10px; text-align: left; padding-left: 10px;">
			<input name="sendMessage" type="submit" value="Send message" class="Icons">
		</div>
		</form>
	</div>
	
	<div id="errorDIV" class="PopUp">
		Invalid recipient !!! 
	</div>
	<% 
		if (request.getParameter("o") != null) {
			int openMessage = Integer.parseInt(request.getParameter("o"));
			%>
			<script>
				<%
					int messageID = Integer.parseInt(request.getParameter("m"));
					String body = messagesBody.get(messageID);
				%>
				document.getElementById("messageBodyDIV").style.visibility='visible';
				document.getElementById("messageParagraph").innerHTML = '<%=body%>';
			</script>
			<% 
		} else {
			%>
			<script>
				document.getElementById("messageBodyDIV").style.visibility='hidden';
			</script>
			<%
		}
	%>
	<%
		if(request.getParameter("e") != null) {
			if(request.getParameter("e").equals("1")) {
				%>
				<script>
					var el = document.getElementById('errorDIV');
					el.style.visibility = 'visible';
					fadeOut(el);
				</script>
				<%
			}
		}
	%>
</body>
</html>