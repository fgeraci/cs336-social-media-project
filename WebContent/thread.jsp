<%@page import="cs336.utilities.Utilities"%>
<%@page import="cs336.utilities.Logger"%>
<%@page import="cs336.controller.Control"%>
<%@page import="java.util.ArrayList" %>
<%@page import="cs336.beans.TopicBean" %>
<%@page import="cs336.beans.ThreadBean" %>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    errorPage="error.jsp"
    %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" href="images/cake_icon.png" />
<title>Harris' Addiction</title>
<link href="styles/main_theme.css" rel="stylesheet">
<link href="styles/logged_style.css" rel="stylesheet">
<link href="styles/menu.css" rel="stylesheet">
<script type="text/javascript" src="scripts/utilities.js"></script>
<script type="text/javascript" src="scripts/logged.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<body>
	<div class="LoggedHeader">
		<div id="logoDIV">
			<img id="miniLogo" src="images/mini_logo.png" alt="mini logo">
		</div>
		<div class="Options">
			<span id="searchSPAN">
				<input type="text" name="searchField" placeholder="search">
				<a href="call the search servlet"><img class="Icons" src="images/search_icon.png" alt="search"></a>
			</span>
			<a href="messages.jsp?c=1">
			<%
			try {
				Cookie c = null;
				c = Control.getCookie(request.getCookies(), "user_id");
			if(c != null) {
				if(Control.hasUnreadMessages(Integer.parseInt(c.getValue()))) {
			%>
						<img class="Icons" id="messagesIcon" src="images/new_message_icon.png" alt="messages">
			<%
					} else {
			%>
						<img class="Icons" id="messagesIcon" src="images/messages_icon.png" alt="messages">
			<%
					}
				} 
			} catch( Exception e) {
				response.sendRedirect("index.jsp");
			}
			%>
			
			</a>
			<img class="Icons" id="gearIcon" src="images/gear.png" alt="settings">
			<div class="PopUp" id="menuDIV">
				<img src="images/x_orange.png" alt="close" class="Icons" id="closeMenuDIV">
				<div style='margin-top: 10px; text-align: center;'>
					<img class="Icons" src="images/profile_icon.png" alt="profile">
					<a href="Logout"><img class="Icons" src="images/logout_icon.png" alt="logout"></a>
					<%
					String value = null;
					boolean admin = false;
					boolean sales = false;
					boolean moderator = false;
					boolean endUser = false;
					String username = "";
					try {
						value = Control.getCookie(request.getCookies(), "role_name").getValue();
						sales = Control.isSales(value);
						admin = Control.isManager(value);
						moderator = Control.isModerator(value);
						endUser = Control.isEndUser(value);
						username = Control.getCookie(request.getCookies(), "user_name").getValue();
						if(admin || sales) {
							Logger.log("Administrator just logged", Logger.INFO);
							%><a href=" "><img class="Icons" src="images/sales_icon_text.png" alt="ads"></a><%
							if(admin) {
								%><a href="Manager"><img class="Icons" src="images/users_icon_text.png" alt="admin"></a><%
							}
						}
					} catch (Exception e) { 
						Logger.log("No cookies available, only acceptable if testing  "+e.getMessage(), Logger.ERROR);
						// response.sendRedirect("index.jsp");
					}
					%>
				</div>
			</div>
		</div>
	</div> 
		<div class="PageBody">
		<span style="vertical-align: middle;">
		<!-- <img src="images/topics_logo.png" alt="Threads"> -->
		Threads
		<%
			try {
				if(moderator || endUser) {
					%>
						<img class="Icons" id="addThread" src="images/add_user_icon.png" alt="Edit" onclick="showAddThread();">
					<%
				}
			} catch (Exception e) {
				Logger.log("No cookies were found, only acceptable in testing.", Logger.ERROR);
			}
		%>
		</span>
		<div style="margin: 0; width: 100%; text-align: right;">
		</div>
		<br>
		<br>
		<div class="PopUp" id="addThreadDIV">
			<div style="margin: auto; width:100%;text-align: left;">
			<img src="images/x_orange.png" alt="close" class="Icons" id="closeAddThread" onclick="hideAddThread();">
			</div>
			<form action="Manager" method="POST" style="margin-right: 15px;">
				<input name="topicID" type="hidden" value="<%= request.getParameter("t") %>">
				<input type="text" style="display:block;" placeholder="New thread name..." name="newThreadName">
				<br>
				<input type="submit" class="Icons" value="Create thread" name="createThread">
			</form>
		</div>
		<%
			int topicID1 = Integer.parseInt(request.getParameter("t"));
			HashMap<Integer,ThreadBean> threads = Control.getAllThreadsFromTopic(topicID1);
			
			for(Map.Entry<Integer, ThreadBean> entry : threads.entrySet()) {
				String threadCreator = Control.getUserNameFromId(entry.getValue().getCreatorID());

				%>
					<form id="threadForm" method="POST" action="Manager">
					<input type="submit" name="voteUpThread" value="<%= entry.getValue().getThreadID() %>" class="thumbsupIcon Icons"> 
					<input type="submit" name="voteDownThread" value="<%= entry.getValue().getThreadID() %>" class="thumbsdownIcon Icons">
					<h1><%= Control.getOverallThreadVotes(entry.getValue().getThreadID()) %></h1>
					
					<div class = "Icons" style = "display:inline-block;">
						
					</div>
					<div  class="ThreadDisplay" style="display:inline-block;">
						<input name="topicID" type="hidden" value="<%= request.getParameter("t") %>">
						<input name="threadID" type="hidden" value="<%= entry.getValue().getThreadID() %>">
						<input name="openThread" class="TopicInput Icons" type="submit" value="<%= entry.getValue().getThreadName().toUpperCase() %>">
					</div>
					<%
						if(admin || moderator || threadCreator.equalsIgnoreCase(username)) {
							%>
								<div class="Icons" style="display:inline-block;">
									<input type="submit" name="deleteThread" value="<%= entry.getValue().getThreadID() %>" class="deleteIcon Icons">
									<input type="image" src="images/pencil_icon.png" name="modifyThreadName" value="<%= entry.getValue().getThreadID() %>" class="modifyIcon Icons" onclick="return showModThreadName(this)">
								</div>
							<%
						}
						%>
				</form>
				<%
			}
		%>
		<div id="modNameDIV" class="PopUp" style='text-align: right;'>
		<img src="images/x_orange.png" alt="close" class="Icons" onclick="hideModThreadName()">
				<div style='text-align: center;'>
					<input form="threadForm" type="text" name="newThreadName" placeholder="New thread name..." style="margin: 10px;" >
					<input
						form="threadForm"
						style="width: auto;color:rgba(0,0,0,0); background: url('images/check_icon.png') no-repeat; box-shadow: none; margin-right: 15px;" 
						id="newThreadNameID" 
						name="modifyThreadName"
						type="submit" class="Icons" value="Change name" > 
				</div> 
		</div>
	</div>
</body>
</html>