<%@page import="cs336.utilities.Logger"%>
<%@page import="cs336.controller.Control"%>
<%@page import="cs336.beans.UserBean" %>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
	errorPage="error.jsp"
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" href="images/cake_icon.png" />
<title>Harris' Addiction</title>
<link href="styles/main_theme.css" rel="stylesheet">
<link href="styles/logged_style.css" rel="stylesheet">
<link href="styles/menu.css" rel="stylesheet">
<link href="styles/manager_style.css" rel="stylesheet">
<link href="styles/table_theme.css" rel="stylesheet">
<script type="text/javascript" src="scripts/utilities.js"></script>
<script type="text/javascript" src="scripts/logged.js"></script>
<script type="text/javascript" src="scripts/manager.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manager Selection</title>

</head>
<body>
	<%
		// secure access
		if(!(Control.validateCookie(request))) response.sendRedirect("index.jsp");
	%>
	<div class="LoggedHeader">
		<div id="logoDIV">
			<img id="miniLogo" src="images/mini_logo.png" alt="mini logo">
		</div>
		<div class="Options">
			<a href="main.jsp">
			<img class="Icons" id="homeIcon" src="images/home_icon.png" alt="home">
			</a>
			<a href="messages.jsp?c=1">
			<img class="Icons" id="messagesIcon" src="images/messages_icon.png" alt="messages">
			</a>
			<img class="Icons" id="gearIcon" src="images/gear.png" alt="settings">
			<div class="PopUp" id="menuDIV">
				<img src="images/x_orange.png" alt="close" class="Icons" id="closeMenuDIV">
				<div style='margin-top: 10px; text-align: center;'>
					<img style='margin: 10px;' class="Icons" src="images/profile_icon.png" alt="profile">
					<a href=" "><img class="Icons" src="images/sales_icon_text.png" alt="ads"></a>
					<a href="Logout"><img style='margin: 10px;' class="Icons" src="images/logout_icon.png" alt="logout"></a>
				</div>
			</div>
		</div>
	</div> 
	<div id="usersDIV">
		<div id="searchDIV">
			<form action="Manager" method="POST">
				<input type="search" placeholder="search message..." name="searchString">
				<input style="box-shadow: none; margin-left: 10px; background: url('images/mini_search_icon.png') no-repeat; color: rgba(0,0,0,0); width: 21px;" name="search" type="submit" class="Icons">
				<img id="addUser" src="images/add_user_icon.png" class="Icons" alt="add user" onclick="return showAddUser.call(this,event)">
			</form>
			<div id="addUserDIV" class="PopUp">
				<img src="images/x_orange.png" alt="close" class="Icons" id="closeUDIV" onclick="closeUserDIV.call(this,event)">
				<form id="registerForm" method="POST" action="Manager">
					<span id="singupSPAN">
					<input class="registerInput" type="text" name="first_name" placeholder="* First Name" required>
					<input class="registerInput" type="text" name="last_name" placeholder="Last Name">
					<br>
					<input id="emailInput" class="registerInput" type="email" name="email" placeholder="* Email Address" required>
					<br>
					<input class="registerInput" type="text" name="user_id" placeholder="* User Name" required>
					<input id="passwordField" class="registerInput" type="password" oninput="validate(this)" name="password" placeholder="* Password" required>
					<br>
					<input class="registerInput" id="confPassword" type="password" name="confpassword" placeholder="* Confirm Password" required>
					<select id="comboBox" name="role_name">
					<%
					ArrayList<String> roles = Control.getAllRoles(); 
					for(String role : roles) {
						%>
							<option value="<%=role%>"><%=role%></option>
						<%
					}
					%>
					</select>
					<span id="registerError"></span>
						<br>
						<input type="submit" name="addUser" value="Add User" class="Icons" id="registerButton">
					</span>
				</form>
			</div>
		</div>
		<div class="PopUp" id="modifyUserDIV">
				<img src="images/x_orange.png" alt="close" class="Icons" id="closeUDIV" onclick="closeModifyUser.call(this,event)">
				<br>
				<br>
				<form action="Manager" method="POST">
				<input id="passwordField" class="registerInput" type="password" name="modPassword" placeholder="New password">
					<br>
				<input class="registerInput" id="confPassword" type="password" name="modPasswordConf" placeholder="Confirm new password">
					<br>
				<select id="modifyRoleCombo" name="modRoleName">
					<%
					roles = Control.getAllRoles(); 
					for(String role : roles) {
						%>
							<option value="<%=role%>"><%=role%></option>
						<%
					}
					%>
				</select>
					<input name="modify" type="submit" value="update" class="Icons" id="modSubmit">
				</form>
		</div>
		<table id="usersTable">
			<tr>
				<th>User Id</th>
			 	<th>User Name</th>
			  	<th>First Name</th>
			  	<th>Last Name</th>
			  	<th>Email</th> 
			  	<th>Role</th>
			  	<th>Points</th>
			  	<th>Actions</th>
			</tr>
			<%
			try {
				ArrayList<UserBean> users = null;
				if(request.getParameter("search") != null) {
					users = Control.trimUsers(request.getParameter("search"));
				} else {
					users = Control.retrieveAllUsers();
				}
				for(UserBean user : users) {
					%>
					<tr>
						<td><%= user.getUserId()  %> </td>
						<td><%= user.getUserName() %> </td>
						<td><%= user.getFirstName() %> </td>
						<td>	<%
								if(!(user.getLastName() == null)) {
									%><%= user.getLastName()%><%
								}
								%> </td>
						<td><%= user.getEmail() %> </td>
						<td><%= user.getRole() %> </td>
						<td><%= user.getRanking() %> </td>
						<td>
							<form action="Manager" method="POST">
							<div>
								<%
								int val = Integer.parseInt(Control.getCookie(request.getCookies(), "user_id").getValue());
								if(user.getUserId() != val && !(user.getUserName().equalsIgnoreCase("admin"))) {
								%>
								<input id="deleteUserInput" name="delete" value="<%= user.getUserId()%>" class="Icons" type="submit">
								<input id="showModInput" style="width: auto;" type="image" src="images/modif_icon.png" value="<%=user.getUserId()%>" onclick="javascript:return showModifyUser(this);"> 
							</div>
							</form>
							<%
							}
							%>
						</td>
					</tr>
					<%
				}
			} catch (Exception e) {
				Logger.log("Users table error", Logger.ERROR);
				// something went really wrong with the DB
			}
			%>
			</table>
			
	</div>
	<%@ include file="advert.jsp" %>
</body>
</html>