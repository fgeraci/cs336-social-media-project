
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
 	errorPage="error.jsp"
    %>

<!DOCTYPE html>
<html>
<head>
<title>HBA Welcome</title>
<link rel="shortcut icon" href="images/cake_icon.png" />
<link href="styles/main_theme.css" rel="stylesheet" />
<script type="text/javascript" src="scripts/index.js"></script>
<script type="text/javascript" src="scripts/utilities.js"></script>
<meta charset="UTF-8">
<title>Harris' Addiction</title>
</head>
<body>
<!-- Trigger cookie reading attempt -->
	<!-- Login and social icons / links -->
	<!-- 
		By creating a form section, we can indicate the 'action' which refers to a Servlet which will
		be called on the SUBMIT typed input click.
	 -->
	<form class="LoginHeader" method="POST" action="Login">
		<span>
			<input type="text" name="user_name" placeholder="User Name / Email" required>
			<input type="password" name="password" placeholder="Password" required>
			<br>
			<span id="loginError"></span>
			<input type="submit" value="Log in" class="buttonWhite" id="loginButton"> <br>
			<input type="submit" id="forgotPassword" value="I forgot my password...">	
		</span>
		<!-- i will include this in the page's body.  
		<a id="createAccount" href="index.html">Register</a>
		-->	
	</form>
	<hr class="FloatingBorder"/>
	<!-- Title header -->
	<div class="Title">
		<span id="logo"><img id="logoImage" src="images/logo4.png" alt="logo"/></span>
	</div>
	<hr class="FloatingBorder"/>
	<div id="conceptDIV">
		<span>
			Join the community.<br>
			Follow your favorite recipes. <br>
			Post your creations.<br>
			Just bake.
		</span>
	</div>
	<!--  <div id="singupDIV"> -->
		<form id="registerForm" method="POST" action="Register">
			<label id="registerLabel">Register</label>
			<br>
			<span id=singupSPAN>
			<input class="registerInput" type="text" name="first_name" placeholder="* First Name" required>
			<input class="registerInput" type="text" name="last_name" placeholder="Last Name">
			<br>
			<input id="emailInput" class="registerInput" type="email" name="email" placeholder="* Email Address" required>
			<br>
			<input class="registerInput" type="text" name="user_id" placeholder="* User Name" required>
			<input id="passwordField" class="registerInput" type="password" oninput="validate(this)" name="password" placeholder="* Password" required>
			<br>
			<input class="registerInput" id="confPassword" type="password" name="confpassword" placeholder="* Confirm Password" required>
			<span id="registerError"></span>
			<br>
			<input type="submit" value="Create Account" class="buttonWhite" id="registerButton">
			</span>
		</form>
</body>
</html>