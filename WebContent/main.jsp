<%@page import="cs336.utilities.Utilities"%>
<%@page import="cs336.utilities.Logger"%>
<%@page import="cs336.controller.Control"%>
<%@page import="java.util.ArrayList" %>
<%@page import="cs336.beans.TopicBean" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    errorPage="error.jsp"
    %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" href="images/cake_icon.png" />
<title>Harris' Addiction</title>
<link href="styles/main_theme.css" rel="stylesheet">
<link href="styles/logged_style.css" rel="stylesheet">
<link href="styles/menu.css" rel="stylesheet">
<script type="text/javascript" src="scripts/utilities.js"></script>
<script type="text/javascript" src="scripts/logged.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>
<body>
	<div class="LoggedHeader">
		<div id="logoDIV">
			<img id="miniLogo" src="images/mini_logo.png" alt="mini logo">
		</div>
		<div class="Options">
			<span id="searchSPAN">
				<input type="text" name="searchField" placeholder="search">
				<a href="call the search servlet"><img class="Icons" src="images/search_icon.png" alt="search"></a>
			</span>
			<a href="messages.jsp?c=1">
			<%
			
			try {
				Cookie c = null;
				c = Control.getCookie(request.getCookies(), "user_id");
			if(c != null) {
				if(Control.hasUnreadMessages(Integer.parseInt(c.getValue()))) {
			%>
						<img class="Icons" id="messagesIcon" src="images/new_message_icon.png" alt="messages">
			<%
					} else {
			%>
						<img class="Icons" id="messagesIcon" src="images/messages_icon.png" alt="messages">
			<%
					}
				} 
			} catch( Exception e) {
				response.sendRedirect("index.jsp");
			}
			%>
			
			</a>
			<img class="Icons" id="gearIcon" src="images/gear.png" alt="settings">
			<div class="PopUp" id="menuDIV">
				<img src="images/x_orange.png" alt="close" class="Icons" id="closeMenuDIV">
				<div style='margin-top: 10px; text-align: center;'>
					<img class="Icons" src="images/profile_icon.png" alt="profile">
					<a href="Logout"><img class="Icons" src="images/logout_icon.png" alt="logout"></a>
					<%
					String value = null;
					boolean admin = false;
					boolean sales = false;
					boolean moderator = false;
					try {
						value = Control.getCookie(request.getCookies(), "role_name").getValue();
						sales = Control.isSales(value);
						admin = Control.isManager(value);
						moderator = Control.isModerator(value);
						if(admin || sales) {
							Logger.log("Administrator just logged", Logger.INFO);
							%><a href="sales.jsp"><img class="Icons" src="images/sales_icon_text.png" alt="ads"></a><%
							if(admin) {
								%><a href="Manager"><img class="Icons" src="images/users_icon_text.png" alt="admin"></a><%
							}
						}
					} catch (Exception e) { 
						Logger.log("No cookies available, only acceptable if testing  "+e.getMessage(), Logger.ERROR);
						// response.sendRedirect("index.jsp");
					}
					%>
				</div>
			</div>
		</div>
	</div> 
	<div class="PageBody">
		<span style="vertical-align: middle;">
		<img src="images/topics_logo.png" alt="Topics">
		<%
			try {
				if(moderator) {
					%>
						<img class="Icons" id="addTopic" src="images/add_user_icon.png" alt="Edit" onclick="showAddTopic();">
					<%
				}
			} catch (Exception e) {
				Logger.log("No cookies were found, only acceptable in testing.", Logger.ERROR);
			}
		%>
		</span>
		<div style="margin: 0; width: 100%; text-align: right;">
			<form action="Manager" method="POST">
			<select
				name="sortTopicsValue" 
				style="font-family: Keen; font-size: 110%;border-radius: 5px;">
				<option value=""></option>
				<option value="alphabetically">Alphabetically</option>
				<option value="hightolow">Popularity - High to Low</option>
				<option value="lowtohigh">Popularity - Low to High</option>
			</select>
			<input 
				type="submit" 
				class="Icons"
				style="box-shadow:none;width:22px;color:rgba(0,0,0,0);background: url('images/sort_icon.png') no-repeat; margin-right: 15px;"
				name="sortTopics">
			</form>
		</div>
		<br>
		<br>
		<div class="PopUp" id="addTopicDIV">
			<div style="margin: auto; width:100%;text-align: left;">
			<img src="images/x_orange.png" alt="close" class="Icons" id="closeAddTopic" onclick="hideAddTopic();">
			</div>
			<form action="Manager" method="POST" style="margin-right: 15px;">
				<input type="text" style="display:block;" placeholder="New topic name..." name="newTopicName">
				<br>
				<input type="submit" class="Icons" value="Create topic" name="createTopic">
			</form>
		</div>
		
		<%
			ArrayList<TopicBean> topics = null;;
			String sbParam = "";
			if(request.getParameter("sb") != null) {
				sbParam = request.getParameter("sb");
				if(sbParam.equals("1")) {
					topics = Control.retrieveAllTopics("topic_name");
				} else if (sbParam.equals("2")) {
					topics = Control.retrieveAllTopics("popularity DESC");
				} else if (sbParam.equals("3")) {
					topics = Control.retrieveAllTopics("popularity");
				}
			} else {
				topics = Control.retrieveAllTopics("NULL");
			}
			for(TopicBean t : topics) {	
				%>
					<form id="topicForm" method="POST" action="Manager">
					<div  class="TopicDisplay" style="display:inline-block;">
						<input name="topicID" type="hidden" value="<%= t.getTopicID() %>">
						<input name="topicName" type="hidden" value="<%= t.getTopicName() %>">
						<input name="openTopic" class="TopicInput Icons" type="submit" value="<%= t.getTopicName().toUpperCase() %>">
					</div>
					<%
						if(admin || moderator) {
							%>
								<div class="Icons" style="display:inline-block;">
									<input type="submit" name="deleteTopic" value="<%= t.getTopicID() %>" class="deleteIcon Icons">
									<input type="image" src="images/pencil_icon.png" name="modifyTopicName" value="<%= t.getTopicID() %>" class="modifyIcon Icons" onclick="return showModTopicName(this)">
									<!-- 
									<input type="submit" name="promoteTopicName" value="<%= t.getTopicID() %>" class="upIcon Icons">
									<input type="submit" name="demoteTopicName" value="<%= t.getTopicID() %>" class="downIcon Icons">
									 -->
								</div>
							<%
						}
						%>
				</form>
				<%
			}
		%>
					
		<div id="modNameDIV" class="PopUp" style='text-align: right;'>
		<img src="images/x_orange.png" alt="close" class="Icons" onclick="hideModTopicName()">
				<div style='text-align: center;'>
					<input form="topicForm" type="text" name="newTopicName" placeholder="New topic name..." style="margin: 10px;" >
					<input
						form="topicForm"
						style="width: auto;color:rgba(0,0,0,0); background: url('images/check_icon.png') no-repeat; box-shadow: none; margin-right: 15px;" 
						id="newTopicNameID" 
						name="modifyTopicName"
						type="submit" class="Icons" value="Change name" > 
				</div> 
		</div>
	</div>
	<%--This is the where the ad is coming from --%>
	<%@ include file="advert.jsp" %>
</body>
</html>