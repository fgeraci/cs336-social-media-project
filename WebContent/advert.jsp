<%@page import="cs336.utilities.Logger"%>
<%@page import="cs336.utilities.Utilities" %>
<%@page import="cs336.controller.Control"%>
<%@page import="cs336.beans.UserBean" %>
<%@page import="cs336.beans.ProductBean" %>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
	errorPage="error.jsp"
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<style>
#comm
{
position:fixed;
left:200px;
top:6px;
width: 35%;
z-index:101;
}
</style>
<body>
	<%
		int userIDTwo = Integer.parseInt(Control.getCookie(request.getCookies(), "user_id").getValue());
		int valueTwo;
	    String c_keywords = Control.getCustomerKeywords(userIDTwo);
	    String adString = null;
	    /*If the customer has keywords, give him an ad*/
		if(c_keywords != null){
	    	ArrayList<String> words = Utilities.getList(c_keywords);
	    	String chosenWord = null;
	    	int sizeOfArray = words.size();
			Random rand = new Random();
			if(sizeOfArray > 1)
				valueTwo = rand.nextInt(sizeOfArray-1);
			else
				valueTwo = 0;
	    	chosenWord = words.get(valueTwo);
			ArrayList<ProductBean> productsTwo = Control.retrieveAllProducts();
			if(productsTwo != null){
				for(ProductBean productTwo : productsTwo){
					if(productTwo.getKeywords().contains(chosenWord)){	
						adString = productTwo.getPicture();
						break;
					}
				}
			}
			else{
				adString = "noad.png";	
			}
		}
		else{
			ArrayList<ProductBean> productsTwo = Control.retrieveAllProducts();
			if(productsTwo.size() > 0){
				int sizeOfArray = productsTwo.size();
				Random rand = new Random();
				if(sizeOfArray > 1)
					valueTwo = rand.nextInt(sizeOfArray) + 1;
				else
					valueTwo = 0;
				for(ProductBean productTwo : productsTwo){
					if(valueTwo == 0){
						adString = productTwo.getPicture();
						break;
					}
					else if(productTwo.getProductID() == valueTwo){	
						adString = productTwo.getPicture();
						break;
					}
					else{}
				}
			}
		}	
	%>    
	<%if(adString == null){adString = "noad.png";}%>
	<%if(adString.equals("noad.png")){ %>
			<a href = "main.jsp"><img id ="comm" src ="images/<%=adString%>" width ="400" height="70" /> </a>
	<%}else{%>
		<a href = "purchase.jsp?ad=<%=adString%>"><img id ="comm" src ="images/<%=adString%>" width ="400" height="70" /> </a>
	<%}%>		
</body>
</html>