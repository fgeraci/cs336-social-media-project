<%@page import="cs336.utilities.Logger"%>
<%@page import="cs336.controller.Control"%>
<%@page import="cs336.beans.UserBean" %>
<%@page import="cs336.beans.ProductBean" %>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
	errorPage="error.jsp"
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" href="images/cake_icon.png" />
<title>Harris' Addiction</title>
<link href="styles/main_theme.css" rel="stylesheet">
<link href="styles/logged_style.css" rel="stylesheet">
<link href="styles/menu.css" rel="stylesheet">
<link href="styles/manager_style.css" rel="stylesheet">
<link href="styles/table_theme.css" rel="stylesheet">
<script type="text/javascript" src="scripts/utilities.js"></script>
<script type="text/javascript" src="scripts/logged.js"></script>
<script type="text/javascript" src="scripts/manager.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manager Selection</title>
</head>
<style>
div.pos_fixed
{
position:fixed;
top:300px;
right:500px;
z = 100;
}
</style>
<body>
	<%
		// secure access
		if(!(Control.validateCookie(request))) response.sendRedirect("index.jsp");
	%>
	<div class="LoggedHeader">
		<div id="logoDIV">
			<img id="miniLogo" src="images/mini_logo.png" alt="mini logo">
		</div>
		<div class="Options">
			<a href="main.jsp">
			<img class="Icons" id="homeIcon" src="images/home_icon.png" alt="home">
			</a>
			<a href="messages.jsp?c=1">
			<img class="Icons" id="messagesIcon" src="images/messages_icon.png" alt="messages">
			</a>
			<img class="Icons" id="gearIcon" src="images/gear.png" alt="settings">
			<div class="PopUp" id="menuDIV">
				<img src="images/x_orange.png" alt="close" class="Icons" id="closeMenuDIV">
				<div style='margin-top: 10px; text-align: center;'>
					<img style='margin: 10px;' class="Icons" src="images/profile_icon.png" alt="profile">
					<a href=" "><img class="Icons" src="images/sales_icon_text.png" alt="ads"></a>
					<a href="Logout"><img style='margin: 10px;' class="Icons" src="images/logout_icon.png" alt="logout"></a>
				</div>
			</div>
		</div>
	</div>
	<% 
	String adKeywords = Control.getAdKeywords(request.getParameter("ad"));
	int userID = Integer.parseInt(Control.getCookie(request.getCookies(), "user_id").getValue());
	String userKeywords = Control.getCustomerKeywords(userID);
	if(userKeywords == null){
		userKeywords = adKeywords;
	}
	else{
		userKeywords = userKeywords + "," + adKeywords;
	}
	Control.updateUserKeywords(userID,userKeywords);
	%>
	<%
	String input = request.getParameter("ad");
	String price = Control.getPrice(input);
	request.setAttribute("price", price);
	%>
	<div class = "pos_fixed">
		<p> This item is ${price} would you like to purchase it?</p>
		<p> <a href="yesPur.jsp?ad=<%=request.getParameter("ad")%>">Yes</a> </p>
		<p> <a href="main.jsp">No</a> </p>
	</div>	
	<%@ include file="advert.jsp" %>
</body>	