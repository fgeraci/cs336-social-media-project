<%@page import="cs336.utilities.Logger"%>
<%@page import="cs336.controller.Control"%>
<%@page import="cs336.beans.UserBean" %>
<%@page import="cs336.beans.ProductBean" %>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
	errorPage="error.jsp"
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" href="images/cake_icon.png" />
<title>Harris' Addiction</title>
<link href="styles/main_theme.css" rel="stylesheet">
<link href="styles/logged_style.css" rel="stylesheet">
<link href="styles/menu.css" rel="stylesheet">
<link href="styles/manager_style.css" rel="stylesheet">
<link href="styles/table_theme.css" rel="stylesheet">
<script type="text/javascript" src="scripts/utilities.js"></script>
<script type="text/javascript" src="scripts/logged.js"></script>
<script type="text/javascript" src="scripts/manager.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manager Selection</title>
</head>
<style>
div.pos_fixed
{
position:absolute;
top:100px;
right:700px;
z = 100;
}
</style>
<body>
	<%
		// secure access
		if(!(Control.validateCookie(request))) response.sendRedirect("index.jsp");
	%>
	<div class="LoggedHeader">
		<div id="logoDIV">
			<img id="miniLogo" src="images/mini_logo.png" alt="mini logo">
		</div>
		<div class="Options">
			<a href="main.jsp">
			<img class="Icons" id="homeIcon" src="images/home_icon.png" alt="home">
			</a>
			<a href="messages.jsp?c=1">
			<img class="Icons" id="messagesIcon" src="images/messages_icon.png" alt="messages">
			</a>
			<img class="Icons" id="gearIcon" src="images/gear.png" alt="settings">
			<div class="PopUp" id="menuDIV">
				<img src="images/x_orange.png" alt="close" class="Icons" id="closeMenuDIV">
				<div style='margin-top: 10px; text-align: center;'>
					<img style='margin: 10px;' class="Icons" src="images/profile_icon.png" alt="profile">
					<a href=" "><img class="Icons" src="images/sales_icon_text.png" alt="ads"></a>
					<a href="Logout"><img style='margin: 10px;' class="Icons" src="images/logout_icon.png" alt="logout"></a>
				</div>
			</div>
		</div>
	</div> 
	<div id="usersDIV">
		<div id="searchDIV">
			<form action="Manager" method="POST">
				<input type="search" placeholder="search message..." name="searchString">
				<input style="box-shadow: none; margin-left: 10px; background: url('images/mini_search_icon.png') no-repeat; color: rgba(0,0,0,0); width: 21px;" name="search" type="submit" class="Icons">
				<img id="addUser" src="images/add_user_icon.png" class="Icons" alt="add user" onclick="return showAddUser.call(this,event)">
			</form>
			<div id="addUserDIV" class="PopUp">
				<img src="images/x_orange.png" alt="close" class="Icons" id="closeUDIV" onclick="closeUserDIV.call(this,event)">
				<form id="registerForm" method="POST" action="Manager">
					<span id="singupSPAN">
					<input class="registerInput" type="text" name="product_name" placeholder="* Product Name" required>
					<input class="registerInput" type="text" name="manufacturer" placeholder="* Manufacturer">
					<br>
					<input class="registerInput" type="text" name="picture" placeholder="* Picture Url" required>
					<input class="registerInput" type="number" name="price" placeholder="* Price" required>
					<br>
					<input class="registerInput" type="text" name="keywords" placeholder="* Keywords" required>
					<%
					ArrayList<String> roles = Control.getAllRoles(); 
					
					%>
					</select>
					<span id="registerError"></span>
						<br>
						<input type="submit" name="addProduct" value="Add Product" class="Icons" id="registerButton">
					</span>
				</form>
			</div>
		</div>
		<div class="PopUp" id="modifyUserDIV">
				<img src="images/x_orange.png" alt="close" class="Icons" id="closeUDIV" onclick="closeModifyUser.call(this,event)">
				<br>
				<br>
				<form action="Manager" method="POST">
				<input id="passwordField" class="registerInput" type="password" name="modPassword" placeholder="New password">
					<br>
				<input class="registerInput" id="confPassword" type="password" name="modPasswordConf" placeholder="Confirm new password">
					<br>
				<select id="modifyRoleCombo" name="modRoleName">
					<%
					roles = Control.getAllRoles(); 
					for(String role : roles) {
						%>
							<option value="<%=role%>"><%=role%></option>
						<%
					}
					%>
				</select>
					<input name="modify" type="submit" value="update" class="Icons" id="modSubmit">
				</form>
		</div>
		<table id="usersTable">
			<tr>
				<th>Product Id</th>
			 	<th>Manufacturer</th>
			  	<th>Picture</th>
			  	<th>Keywords</th>
			  	<th>Price</th> 
			</tr>
			<%
			try {
				ArrayList<ProductBean> products = null;
				if(request.getParameter("search") != null) {
					/*products = Control.trimUsers(request.getParameter("search"));*/
				} else {
					products = Control.retrieveAllProducts();
				}
				for(ProductBean product : products) {
					%>
					<tr>
						<td><%= product.getProductID()  %> </td>
						<td><%= product.getManufacturer() %> </td>
						<td><%= product.getPicture() %> </td>
						<td><%= product.getKeywords()%> </td>
						<td><%= product.getPrice() %> </td>
						<td>
							<form action="Manager" method="POST">
							<div>
								<input id="deleteProdInput" name="deleteProd" value="<%= product.getProductID()%>" class="Icons" type="submit">
							</div>
							</form>
						</td>
					</tr>
					<%
				}
			} catch (Exception e) {
				Logger.log("Product table error", Logger.ERROR);
				// something went really wrong with the DB
			}
			%>
			</table>
	</div>	
	<div class = "pos_fixed">
		<p> <a href="reportc.jsp">Customer Report</a> </p>
		<p> <a href="reportp.jsp">Product Report</a> </p>
	</div>	
	<%@ include file="advert.jsp" %>
</body>
</html>