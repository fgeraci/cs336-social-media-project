function showAddUser(event) {
	var el = document.getElementById('addUserDIV');
	el.style.visibility = 'visible';
	return false;
}

function closeUserDIV(event) {
	var el = document.getElementById('addUserDIV');
	el.style.visibility = 'hidden';
}

function showModifyUser(element) {
	console.log(element.value);
	var el2 = document.getElementById('modSubmit');
	console.log('original value: '+el2.value);
	el2.value = element.value;
	console.log('value exchanged, new value: '+el2.value);
	document.getElementById("modifyUserDIV").style.visibility = 'visible';
	return false;
}

function closeModifyUser(event) {
	var el = document.getElementById('modifyUserDIV');
	el.style.visibility = 'hidden';
}
