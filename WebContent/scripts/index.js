document.addEventListener('DOMContentLoaded', function(){
	console.log("DOM Loaded");
	init();
	});

function init() {
	
	console.log("Executing init");
	var query = window.location.search.substring(1);
	if(query == "error") { // plain login error.
		/** login error */
		console.log("Login error");
		var message = document.getElementById('loginError');
		message.style.color = "WHITE";
		message.style.backgroundColor = "RED";
		message.style.borderRadius = "2px";
		message.style.opacity = ".75";
		message.style.padding = "2px";
		document.getElementById('loginError').innerHTML = "INVALID USER OR PASSWORD";
		fadeOut(message);
	} else if (query.indexOf("error") != -1) { // registration error
		/** registering error */
		console.log("Register error");
		var message = document.getElementById('registerError');
		message.style.color = "WHITE";
		message.style.backgroundColor = "RED";
		message.style.borderRadius = "2px";
		message.style.opacity = ".75";
		message.style.padding = "2px";
		document.getElementById('registerError').innerHTML = query == "error2" ? "USER NAME TAKEN" : "PASSWORD MISSMATCH";
		fadeOut(message);
	}
	
	var buttons = document.getElementsByClassName('buttonWhite');
	for(var i = 0, j = buttons.length; i < j; i++) {
		buttons[i].addEventListener('mouseover',
				function(event){
			increaseOpacity(event);
		});
		buttons[i].addEventListener('mouseout',
				function(event){
			decreaseOpacity(event);
		});
	}
	
	var inputs = document.getElementsByClassName("registerInput");
	for(var i =0, j = inputs.length; i < j; i++) {
		inputs[i].addEventListener('focus', function(event){
			clearValue(event);
		});
	}
	
}

function validate(item) {
	var value = item.value;
	console.log(value);
	var pass = /^[A-Za-z0-9]\w{7,14}$/;
	var validity = value.match(pass);
	console.log(validity);
	if(!(value.match(pass))) {
		item.setCustomValidity("Password must be at least 7 characters long, contain at least 1 upper case letter, 1 lower case letter and 1 digit.");
		return false;
	} else {
		item.setCustomValidity('');
		return true;
	}
}