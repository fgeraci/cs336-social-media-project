document.addEventListener('DOMContentLoaded', function(){
	console.log("Logged page DOM Loaded");
	init();
	});

var timeout;

function init() {
	var icons = document.getElementsByClassName('Icons');
	for(var i = 0, j = icons.length; i < j; i++) {
		icons[i].addEventListener('mouseover',
				function(event){
			increaseOpacity(event);
		});
		icons[i].addEventListener('mouseout',
				function(event){
			decreaseOpacity(event);
		});
	}
	
	document.getElementById('gearIcon').addEventListener('mouseover', function(event) {
		var el = document.getElementById('menuDIV');
		el.style.visibility = 'visible';
		el.style.opacity = '1';
	});
	
	document.getElementById('closeMenuDIV').addEventListener('click', function (event) {
		var el = document.getElementById('menuDIV');
		el.style.visibility = 'hidden';
		el.style.opacity = '0';
	});
	try {
	document.getElementById('addTopic').addEventListener('click', function(event) { });
	} catch(e) { }
	try {
	document.getElementById('addThread').addEventListener('click', function(event) { });
	} catch(e) { }
	
	try {
		document.getElementById('addPost').addEventListener('click', function(event) { });
		} catch(e) { }
	try{
		document.getElementById('addComment').addEventListener('click', function(event) {
			
		});
	} catch(e){}
	
}

function showAddTopic() {
	document.getElementById('addTopicDIV').style.visibility = 'visible';
}
function showAddPost() {
	document.getElementById('addPostDIV').style.visibility = 'visible';
}

function showAddThread(){
	document.getElementById('addThreadDIV').style.visibility = 'visible';
}
function showAddComment(element){
	var postid = element.value;
	console.log('Value of post is ID: '+postid);
	document.getElementById('postIDHolder').value = postid;
	document.getElementById('addCommentDIV').style.visibility = 'visible';
	return false;
}

function hideAddTopic() {
	document.getElementById('addTopicDIV').style.visibility = 'hidden';
}
function hideAddThread() {
	document.getElementById('addThreadDIV').style.visibility = 'hidden';
}
function hideAddComment() {
	document.getElementById('addCommentDIV').style.visibility = 'hidden';
}
function hideAddPost() {
	document.getElementById('addPostDIV').style.visibility = 'hidden';
}
function showModTopicName(element) {
	console.log('current topic id = '+element.value); // working
	document.getElementById('newTopicNameID').value = element.value;
	console.log('value modified to: '+document.getElementById("newTopicNameID").value);
	document.getElementById('modNameDIV').style.visibility = 'visible';
	return false;
}
function showModThreadName(element) {
	console.log('current thread id = '+element.value); // working
	document.getElementById('newThreadNameID').value = element.value;
	console.log('value modified to: '+document.getElementById("newThreadNameID").value);
	document.getElementById('modNameDIV').style.visibility = 'visible';
	return false;
}
function showModPost(element) {
	console.log('current post id = ' + element.value); // working
	document.getElementById('newPostID').value = element.value;
	console.log('value modified to: '+document.getElementById("newPostID").value);
	document.getElementById('modPostDIV').style.visibility = 'visible';
	return false;
}
function showModComment(element) {
	console.log('current comment post id = ' + element.value); // working
	document.getElementById('newCommentID').value = element.value;
	console.log('value modified to: '+document.getElementById("newCommentID").value);
	document.getElementById('modCommentDIV').style.visibility = 'visible';
	return false;
}

function hideModTopicName() {
	document.getElementById('modNameDIV').style.visibility = 'hidden';
}
function hideModPost() {
	document.getElementById('modPostDIV').style.visibility = 'hidden';
}

function hideModComment() {
	document.getElementById('modCommentDIV').style.visibility = 'hidden';
}

function hideModThreadName() {
	document.getElementById('modNameDIV').style.visibility = 'hidden';
}
 