function openMessage(rid,unread,body,form) {
	console.log(unread+" "+rid);
	if(unread == 1) {
		console.log('Open message '+rid+' true');
		return true;
	} else {
		try {
			for(i=0; i < form.parentNode.childNodes.length ; i++){
			    var currNode=form.parentNode.childNodes[i];
			    if(currNode.name == 'sender') {
			        var sender = currNode.value;
			    	console.log("childNodes[" + i + "]: " + currNode);
			    }             
			}
			document.getElementById('destinationInput').value = sender;
			console.log('Currently viewing message from form: '+form);
		} catch(e) { }
		document.getElementById("messageParagraph").innerHTML = body;
		var el = document.getElementById('newMessageDIV');
		if(el.style.visibility == 'visible') makeInvisible(el);
		document.getElementById('messageBodyDIV').style.visibility = 'visible';
		return false;
	}
	console.log('this should not happen');
	return false;
}

function replyMessage() {
	document.getElementById('subjectInput').value = '';
	openNewMessage(null);
}

function openNewMessage(el) {
	try {
		if(el.id = 'newMessageLi') {
			document.getElementById('destinationInput').value = '';
			document.getElementById('subjectInput').value = '';
		}
	} catch(e) {}
	var el2 = document.getElementById('messageBodyDIV');
	if(el2.style.visibility == 'visible') makeInvisible(el2);
	document.getElementById('newMessageDIV').style.visibility = 'visible';
}

function closeNewMessage() {
	try {
		document.getElementById('destinationInput').value = '';
		document.getElementById('subjectInput').value = '';
	} catch(e) { }
	document.getElementById('newMessageDIV').style.visibility = 'hidden';
}

function closeMessage(element) {
	try {
		document.getElementById('destinationInput').value = '';
		document.getElementById('subjectInput').value = '';
	} catch (e) {}
	document.getElementById('messageBodyDIV').style.visibility = 'hidden';
	return false;
}

function makeInvisible(element) {
	element.style.visibility = 'hidden';
}