/* UTILITIES SCRIPTS */

var timeout;

function increaseOpacity(event) {
	clearTimeout(timeout);
  var el = (event.target || event.srcElement);
  var op = window.getComputedStyle(el,null).opacity;
  if((Math.round((Number(op)*10)))/10 <= 0.9) {
    var val = Number(op)+0.1;
    el.style.opacity = val.toString();
    timeout = setTimeout(function(){increaseOpacity(event);},50);
  }
}

function decreaseOpacity(event) {
	  clearTimeout(timeout);
	  var el = (event.target || event.srcElement);
	  var op = window.getComputedStyle(el,null).opacity;
	  if((Math.round((Number(op)*10)))/10 >= 0.7) {
	    var val = Number(op)-0.1;
	    el.style.opacity = val.toString();
	    timeout = setTimeout(function(){decreaseOpacity(event);},50);
	  }
}

function clearValue(event) {
	var type = event.type;
	var el = (event.target || event.srcElement);
	if(type == 'blur') {
		el.style.color = "GRAY";
	} else {
		el.style.color = 'BLACK';
	}
}

/**
 * DEPRECATED, do not use it
 */

var timeout2;

function fadeOut(elem) {
	clearTimeout(timeout2);
	var val = window.getComputedStyle(elem,null).opacity;
	var number = Number(val);
	if(number > 0) { 
		console.log("fade out called on "+elem.id+" with opacity "+val);
		var value = number < 0.1 ? 0 : number - 0.01;
		elem.style.opacity = value.toString();
		timeout2 = setTimeout(function(){fadeOut(elem);},110);
	}
}

function replaceBreaks(text) {
	text.replace(/\r\n/g,'<br />');
}