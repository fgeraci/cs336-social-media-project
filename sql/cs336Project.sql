/*
*	INITIALIZING DB
*/

DROP DATABASE IF EXISTS cs336project;	

CREATE DATABASE cs336project;

USE cs336project;

/*
*
*	GENERATING TABLES: Users, Roles and AssignedRole
*	POPULATING: 	Users(admin), 
*			Roles(EndUser,Administrator,Moderator,Sales),
*			AssignedRole(admin(1),Administrator(2)).
*/

CREATE TABLE Users 
(
	user_id int AUTO_INCREMENT,
	user_name varchar(20) NOT NULL,
	first_name varchar(20) NOT NULL,
	last_name varchar(20),
	email varchar(100) NOT NULL,
	password varchar(50) NOT NULL,
	picture_URL varchar(150),
	keywords varchar(5000),
	ranking float NOT NULL,
	PRIMARY KEY (user_id)
) Engine=InnoDB;

CREATE TABLE LogsCount
(
	user_id int NOT NULL,
	logs_count int DEFAULT 0,
	last_log date,
	FOREIGN KEY (user_id)
		REFERENCES Users(user_id)
		ON DELETE CASCADE
) Engine=InnoDB;

INSERT INTO Users (user_name,first_name,last_name,email,password,picture_URL,ranking)
	VALUES ('admin','admin',NULL,'admin@rockhall.com','adminpwd','images/users_profiles/admin.png',500);
SET @admin_user_id = LAST_INSERT_ID();

INSERT INTO LogsCount VALUES (@admin_user_id,0,NULL);

CREATE TABLE Roles
(
	role_id int AUTO_INCREMENT,
	role_name varchar(25) NOT NULL,
	PRIMARY KEY (role_id)
) Engine=InnoDB;

INSERT INTO Roles (role_name) VALUES ('EndUser');
INSERT INTO Roles (role_name) VALUES ('Administrators');
SET @admin_id = LAST_INSERT_ID();
INSERT INTO Roles (role_name) VALUES ('Moderator');
INSERT INTO Roles (role_name) VALUES ('Sales');

CREATE TABLE AssignedRole
(
	user_id int,
	role_id int,
	FOREIGN KEY (role_id)
		REFERENCES Roles(role_id)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (user_id)
		REFERENCES Users(user_id)
		ON DELETE CASCADE
) Engine=InnoDB;

INSERT INTO AssignedRole (user_id,role_id) VALUES (@admin_user_id,@admin_id);

CREATE TABLE Administrators
(
	user_id int,
	FOREIGN KEY (user_id)
		REFERENCES Users(user_id)
		ON DELETE CASCADE
) Engine=InnoDB;

CREATE TABLE Moderators
(
	user_id int,
	FOREIGN KEY (user_id)
		REFERENCES Users(user_id)
		ON DELETE CASCADE
) Engine=InnoDB;

CREATE TABLE Sales
(
	user_id int,
	FOREIGN KEY (user_id)
		REFERENCES Users(user_id)
		ON DELETE CASCADE
) Engine=InnoDB;

INSERT INTO Administrators VALUES (@admin_user_id);

/*
 * GENERATING TABLES: Messages
 */

CREATE TABLE Messages
(
	message_id int AUTO_INCREMENT,
	owner_id int,
	receiver_id int,
	message_date date NOT NULL,
	subject varchar(70) NOT NULL,
	body varchar(5000) NOT NULL,
	unread int NOT NULL DEFAULT 1,
	trashed int NOT NULL DEFAULT 0,
	PRIMARY KEY (message_id),
	FOREIGN KEY (owner_id)
		REFERENCES Users(user_id)
		ON DELETE CASCADE,
	FOREIGN KEY (receiver_id)
		REFERENCES Users(user_id)
		ON DELETE CASCADE
) Engine=InnoDB;	

/**
 * GENERATING TABLES: Visits
 */

CREATE TABLE Visits /* not as a verb but as counting actual visits */
(
	user_id int,
	click_type ENUM('topic','thread','post'),
	type_id int NOT NULL,
	FOREIGN KEY (user_id) REFERENCES Users(user_id)
) Engine=InnoDB;

/**
 * GENERATING TABLES: Topics, Threads, Posts, Comments
 */

CREATE TABLE Topics
(
	topic_id int AUTO_INCREMENT,
	topic_name varchar(50) NOT NULL,
	number_of_threads int NOT NULL DEFAULT 0,
	popularity int NOT NULL DEFAULT 0,
	PRIMARY KEY (topic_id)
) Engine=InnoDB;

CREATE TABLE Threads
(
	thread_id int AUTO_INCREMENT,
	parent_topic_id int,
	thread_name varchar(50) NOT NULL,
	creator_id int,
	number_of_posts int NOT NULL,
	overall_votes int NOT NULL,
	published_date date NOT NULL,
	rank int NOT NULL,
	PRIMARY KEY (thread_id),
	FOREIGN KEY (parent_topic_id) REFERENCES Topics(topic_id)
		ON DELETE CASCADE,
	FOREIGN KEY (creator_id) REFERENCES Users(user_id)
) Engine=InnoDB;

CREATE TABLE Posts
(
	post_id int AUTO_INCREMENT,
	owner int,
	thread_id int,
	posted_date date NOT NULL,
	parent_post int,
	body varchar(5000) NOT NULL,
	votes int NOT NULL,
	rank int NOT NULL,
	PRIMARY KEY (post_id),
	FOREIGN KEY (owner) REFERENCES Users(user_id),
	FOREIGN KEY (thread_id) REFERENCES Threads(thread_id)
		ON DELETE CASCADE
) Engine=InnoDB;

CREATE TABLE PostsComments
(
	post_id int NOT NULL,
	creator_id int,
	comment varchar(3000) NOT NULL,
	comment_date date NOT NULL,
	FOREIGN KEY (creator_id) REFERENCES Users(user_id),
	FOREIGN KEY (post_id)
		REFERENCES Posts(post_id)
		ON DELETE CASCADE
) Engine=InnoDB;

/**
 * GENERATING TABLES: Search, Searches
 */

CREATE TABLE Search
(
	search_id int AUTO_INCREMENT,
	user_id int,
	thread_id int,
	search_date date NOT NULL,
	search_string varchar(100) NOT NULL,
	category_filter ENUM('date','popularity','relevance'),
	PRIMARY KEY (search_id),
	FOREIGN KEY (user_id) REFERENCES Users(user_id),
	FOREIGN KEY (thread_id) REFERENCES Threads(thread_id)
) Engine=InnoDB;

CREATE TABLE Searches
(
	user_id int,
	search_id int,
	PRIMARY KEY (user_id),
	FOREIGN KEY (user_id) REFERENCES Users(user_id),
	FOREIGN KEY (search_id) REFERENCES Search(search_id)
		ON DELETE CASCADE
) Engine=InnoDB;

/**
 * GENERATING TABLES: Customers, Products
 */

CREATE TABLE Customers
(
	user_id int,
	keywords varchar(2000),
	PRIMARY KEY (user_id),
	FOREIGN KEY (user_id)
		REFERENCES Users(user_id)
		ON DELETE CASCADE
) Engine=InnoDB;

CREATE TABLE Products
(
	product_id int AUTO_INCREMENT,
	manufacturer varchar(25) NOT NULL,
	picture_url varchar(150),
	keywords_list varchar(1500) NOT NULL,
	/*clicked_on int NOT NULL, dk if i need this yet, and now price is key temp */
	price varchar(50) NOT NULL, 
	PRIMARY KEY (product_id)
) Engine=InnoDB;

CREATE TABLE ProductsSalesData
(
	transaction_id int AUTO_INCREMENT,
	product_id int NOT NULL,
	customer_id int NOT NULL,
	price varchar(50) NOT NULL,
	on_sale boolean,
	PRIMARY KEY (transaction_id),
	FOREIGN KEY (product_id)
		REFERENCES Products(product_id) ON DELETE CASCADE,
	FOREIGN KEY (customer_id)
		REFERENCES Customers(user_id)
) Engine=InnoDB;


CREATE TABLE SalesAnalysis 
(
	product_id int,
	items_sold int,
	avg_price float,
	PRIMARY KEY (product_id),
	FOREIGN KEY (product_id) REFERENCES Products(product_id)
) Engine=InnoDB;


/*
 * PROCEDURES
 */

delimiter //

/**
 * Authenticates an user_name and password against Users. Return user_id if true, NULL otherwise. 
 */

CREATE PROCEDURE `authenticateUser` (userName varchar(20), password varchar(50), OUT result INT)
BEGIN
	SELECT U.user_id INTO result FROM Users U WHERE U.user_name=userName AND U.password=password;
	IF (result IS NOT NULL) THEN \
		UPDATE LogsCount L SET L.logs_count=L.logs_count+1, L.last_log=CURRENT_DATE() WHERE L.user_id = result;
	END IF;
END 
//

/**
 * Registers a new user.
 */
CREATE PROCEDURE `registerUser` (	name varchar(20), 
									last_name varchar(20), 
									email varchar(100), 
									password varchar(50), 
									user_name varchar(20),
									OUT user_id INT)
BEGIN
	/*
	 * Validate user_name
	 * register user
	 * return new user_id OR -1
	 */
	SELECT U.user_id INTO user_id FROM Users U WHERE U.user_name=user_name;
	IF (user_id IS NOT NULL) THEN
		/* ERROR , USER WITH SAME USER_NAME */
		SET user_id = -1;
	ELSE
		/* go ahead, user does not exists */
		INSERT INTO Users (user_name,first_name,last_name,email,password,picture_URL,ranking) \
		 	VALUES(user_name,name,last_name,email,password,'images/tab_icon.png',0);
		SET user_id = LAST_INSERT_ID();
		INSERT INTO AssignedRole VALUES(LAST_INSERT_ID(),1);
		/* make the newly registered user a customer */
		INSERT INTO Customers VALUES (LAST_INSERT_ID(),NULL);
		INSERT INTO Messages (owner_id,receiver_id,message_date,subject,body) \
			VALUES (1,LAST_INSERT_ID(),CURRENT_DATE(),'Welcome!!!','We bid you welcome in the name of the team !!!!');
	END IF;
END 
//

/**
 * Retrieves an specific user by its id.
 */
CREATE PROCEDURE `retrieveUser` ( user_id int )
BEGIN
	/* Will return a full description of the user, including its role */
	SELECT * FROM 
		(Users U INNER JOIN 
			(SELECT A.user_id,A.role_id,R.role_name FROM AssignedRole A INNER JOIN Roles R ON A.role_id=R.role_id) T 
			ON U.user_id=T.user_id) WHERE U.user_id=user_id;
END
//

/**
 * Retrieves the ID corresponding to a user name.
 */
CREATE PROCEDURE `fromUserNameToID` (IN userName varchar(50))
BEGIN
	SELECT * FROM Users WHERE user_name=userName;
END
//

/**
 * Retrieves all registered users from every Role.
 */
CREATE PROCEDURE `retrieveAllUsers` ()
BEGIN
	SELECT * FROM 
		(Users U INNER JOIN 
			(SELECT A.user_id,A.role_id,R.role_name FROM AssignedRole A INNER JOIN Roles R ON A.role_id=R.role_id) T 
			ON U.user_id=T.user_id);
END 
//

/**
 * Deletes the specific user.
 */
CREATE PROCEDURE `deleteUser` (IN userID int)
BEGIN
	DELETE FROM Users WHERE user_id=userID;
END
//

/**
 * Return the available Roles' names.
 */
CREATE PROCEDURE `getRoles` ()
BEGIN
	SELECT role_name FROM Roles;
END 
//

/**
 * Updates a user's role.
 */
CREATE PROCEDURE `modifyRole` (IN userID int, IN roleID int)
BEGIN
	UPDATE AssignedRole SET role_id=roleID WHERE user_id=userID;
END 
//

/**
 * Updates an user's password.
 */
CREATE PROCEDURE `updatePassword` (IN userID int, IN newPassword varchar(100))
BEGIN
	UPDATE Users SET password=newPassword WHERE user_id=userID;
END 
//

/**
 * Update user's login info.
 */
CREATE PROCEDURE `registerLogin` (IN userID int)
BEGIN
	UPDATE LogsCount SET last_log=CURDATE(),logs_count=logs_count+1 WHERE user_id=userID;
END 
//

CREATE PROCEDURE `retrieveAllTopics` (IN orderBy varchar(15))
BEGIN
	IF (orderBy IS NULL) THEN
		SELECT * FROM Topics;
	ELSEIF (orderBy = 'topic_name') THEN
		SELECT * FROM Topics ORDER BY topic_name;
	ELSEIF (orderBy = 'popularity') THEN
		SELECT * FROM Topics ORDER BY popularity;
	ELSE
		SELECT * FROM Topics ORDER BY popularity DESC;
	END IF;
END 
//

CREATE PROCEDURE `retrieveAllThreads` (IN orderBy varchar(15))
BEGIN
	IF (orderBy IS NULL) THEN
		SELECT * FROM Threads;
	ELSEIF (orderBy = 'thread_name') THEN
		SELECT * FROM Threads ORDER BY thread_name;
	ELSEIF (orderBy = 'overall_votes') THEN
		SELECT * FROM Threads ORDER BY overall_votes;
	ELSE
		SELECT * FROM Threads ORDER BY rank DESC;
	END IF;
END 
//

CREATE PROCEDURE `addNewTopic`(IN newTopic varchar(100))
BEGIN
	DECLARE var INT DEFAULT 0;
	SELECT topic_id INTO var FROM Topics WHERE topic_name=newTopic;
	IF (var < 1) THEN \
		INSERT INTO Topics (topic_name,number_of_threads,popularity) VALUES (newTopic,0,0);
	END IF;
END 
//

CREATE PROCEDURE `addNewThread`(IN newThread varchar(50), IN parentTopicID INT, IN creatorID INT)
BEGIN
	DECLARE var INT DEFAULT 0;
	DECLARE threadDate date DEFAULT NOW();
	SELECT thread_id INTO var FROM Threads WHERE thread_name = newThread;
	IF (var < 1) THEN \
		INSERT INTO Threads (parent_topic_id, creator_id, thread_name,number_of_posts,overall_votes, published_date, rank) VALUES (parentTopicID, creatorID, newThread,0,0, threadDate, 0);
		UPDATE Topics set number_of_threads = number_of_threads + 1 where topic_id = parentTopicID;
	END IF;
END 
//

CREATE PROCEDURE `addNewPost`(IN newPost varchar(5000), IN parentPostID INT, IN creatorID INT, IN threadID INT)
BEGIN
	DECLARE var INT DEFAULT 0;
	DECLARE postedDate date DEFAULT NOW();
	SELECT post_id INTO var FROM Posts WHERE body = newPost AND parent_post = parentPostID AND owner = creatorID;
	IF (var < 1) THEN \
		INSERT INTO Posts (owner, thread_id, posted_date,parent_post,body, votes, rank) VALUES (creatorID, threadID, postedDate, parentPostID, newPost, 0, 0);
		UPDATE Threads set number_of_posts = number_of_posts + 1 where thread_id = threadID;
	END IF;
END 
//


/**
 * Delets the indicated topic
 */
CREATE PROCEDURE `deleteTopic` (IN topicID INT)
BEGIN
	DELETE FROM Topics WHERE topic_id = topicID;
END 
//

CREATE PROCEDURE `deleteComment` (IN commentID INT)
BEGIN
	DELETE FROM PostsComments WHERE post_id = commentID;
END
//

/**
 * Deletes the indicated thread
 */
CREATE PROCEDURE `deleteThread` (IN threadID INT)
BEGIN
	DECLARE var INT DEFAULT 0;
	SELECT parent_topic_id INTO var FROM Threads WHERE thread_id = threadID;
	DELETE FROM Threads WHERE thread_id = threadID;
	UPDATE Topics set number_of_threads = number_of_threads - 1 WHERE topic_id = var;
END 
//

/**
 * Deletes the indicated post
 */
CREATE PROCEDURE `deletePost` (IN postID INT)
BEGIN
	DECLARE var INT;
	SELECT thread_id INTO var FROM Posts WHERE post_id = postID;  
	DELETE FROM Posts WHERE post_id = postID;
	UPDATE Threads set number_of_posts = number_of_posts - 1 WHERE thread_id = var; 
END 
//

/**
 * Renames a topic as per admin request.
 */
CREATE PROCEDURE `renameTopic` (IN topicID INT, IN newTopicName varchar(150))
BEGIN
	UPDATE Topics SET topic_name=newTopicName WHERE topic_id=topicID;
END 
//

CREATE PROCEDURE `editComment` (IN commentID INT, IN newComment varchar(150))
BEGIN
	UPDATE PostsComments SET comment=newComment WHERE post_id=commentID;
	UPDATE PostsComments SET comment_date = CURRENT_DATE() WHERE post_id = commentID;
END 
//


/**
 * Rename a thread as per admin request
 */
CREATE PROCEDURE `renameThread` (IN threadID INT, IN newThreadName varchar(200))
BEGIN
	UPDATE Threads SET thread_name = newThreadName WHERE thread_id = threadID;
END 
//

/**
 * Edit a post as per user request
 */
CREATE PROCEDURE `modifyPost` (IN postID INT, IN edittedPost varchar(5000))
BEGIN
	UPDATE Posts SET body = edittedPost WHERE post_id = postID;
END 
//

/**
 * Returns a list of unread messages for the specified user.
 */
CREATE PROCEDURE `retrieveUnreadMessages` (IN userID INT)
BEGIN
	SELECT * FROM Messages WHERE receiver_id=userID AND unread=1;	
END
//

/**
 * Retrieve all messages FROM and TO that user.
 */
CREATE PROCEDURE `retrieveMessages` (IN userID INT)
BEGIN
	SELECT * FROM (Messages INNER JOIN (SELECT user_name AS sender,user_id FROM Users) U ON owner_id=user_id);
END 
//

/*
 * Mark a message asread.
 */
CREATE PROCEDURE `markAsRead` (IN messageID INT)
BEGIN
	UPDATE Messages SET unread=0 WHERE message_id=messageID;	
END 
//

/**
 * Deletes the specified message.
 */
CREATE PROCEDURE `deleteMessage` (IN messageID INT)
BEGIN
	DELETE FROM Messages WHERE message_id=messageID;
END 
//

/**
 * Creates a new message for the specified user.
 */
CREATE PROCEDURE `sendMessage` (IN fromID INT, IN toID INT, IN mSubject varchar(150), IN mBody varchar(5000))
BEGIN
	INSERT INTO Messages (owner_id,receiver_id,message_date,subject,body) VALUES (fromID,toID,CURRENT_DATE(),mSubject,mBody);
END
//

/**
 * 
 */
CREATE PROCEDURE `getUserNameFromId` (IN userID INT)
BEGIN
	SELECT user_name FROM Users WHERE user_id=userID;	
END 
//

/**
 * Retrieves an specific topic.
 */
CREATE PROCEDURE `retrieveTopic` (IN topicID INT)
BEGIN
	UPDATE Topics SET popularity=popularity+1 WHERE topic_id=topicID;
	SELECT * FROM Topics WHERE topic_id=topicID;
END 
//

/**
 * Retreieves all the children threads from a specific topic.
 */
CREATE PROCEDURE `retrieveThreads` (IN parentID INT)
BEGIN
	SELECT * FROM Threads WHERE parent_topic_id=parentID ORDER BY published_date;
END 
//

CREATE PROCEDURE `retrievespecificThreads` (IN threadID INT, IN parentTopic INT)
BEGIN
	SELECT * FROM Threads WHERE parent_topic_id = parentTopic AND thread_id = threadID;
END
//

/**
 *  Registers the product into the database
 */
CREATE PROCEDURE `registerProduct` (IN manufacturer varchar(25),IN picture varchar(150),IN price varchar(50), IN keywords varchar(1500),
					OUT product_id INT)
BEGIN
		/* Insert the product */
		INSERT INTO Products (product_id,manufacturer,picture_url,keywords_list,price)
		 	VALUES(LAST_INSERT_ID(),manufacturer, picture, keywords, price);
		SET product_id = LAST_INSERT_ID();
END 
//

/**
 * Retreieves all the children threads from a specific topic.
 */
CREATE PROCEDURE `retrievePosts` (IN parentthreadID INT)
BEGIN
	SELECT * FROM Posts WHERE thread_id=parentthreadID;
END 
//

CREATE PROCEDURE `retrieveComments` (IN parentPost INT)
BEGIN
	SELECT * FROM PostsComments WHERE post_id = parentPost;
END
//

CREATE PROCEDURE `updateVoteforThread` (IN threadID INT)
BEGIN
	UPDATE Threads SET overall_votes = overall_votes + 1 WHERE thread_id = threadID;
END 
//

CREATE PROCEDURE `decreaseVoteforThread` (IN threadID INT)
BEGIN
	UPDATE Threads SET overall_votes = overall_votes - 1 WHERE thread_id = threadID;
END 
//

CREATE PROCEDURE `updateVoteforPost` (IN postID INT)
BEGIN
	UPDATE Posts SET votes = votes + 1 WHERE post_id = postID;
END 
//

CREATE PROCEDURE `decreaseVoteforPost` (IN postID INT)
BEGIN
	UPDATE Posts SET votes = votes - 1 WHERE post_id = postID;
END 
//

CREATE PROCEDURE `addNewComment` (IN parentPost INT, IN commentBody varchar(3000), IN creator INT)
BEGIN
	DECLARE var INT DEFAULT 0;
	DECLARE postedDate date DEFAULT NOW();
	INSERT INTO PostsComments (post_id, comment, comment_date, creator_id) VALUES (parentPost, commentBody, postedDate, creator);
END
//

/*
 * Retrieves all products from products table.
 */
	
CREATE PROCEDURE `retrieveAllProducts` ()
BEGIN
	SELECT * FROM Products;
END 
//

/**
 * Deletes the specific product.
 */
CREATE PROCEDURE `deleteProd` (IN prodID int)
BEGIN
	DELETE FROM Products WHERE product_id=prodID;
END
//

/**
 * Gets price of ad based item
 */

CREATE PROCEDURE `getPrice` (IN ad varchar(150))
BEGIN
	SELECT P.price FROM Products P WHERE P.picture_url = ad;
END
//

/**
 *  Registers the sale into the database
 */
CREATE PROCEDURE `registerSale` (IN userID INT,IN productID INT,IN price_in varchar(50), IN sale BOOLEAN,
					OUT trans_id INT)
BEGIN
		/* Insert the product */
		INSERT INTO ProductsSalesData (transaction_id,product_id,customer_id,price,on_sale)
		 	VALUES(LAST_INSERT_ID(),productID,userID,price_in,sale);
		SET trans_id = LAST_INSERT_ID();
END 
//

CREATE PROCEDURE `getProduct` (IN ad varchar(150))
BEGIN
	SELECT P.product_id FROM Products P WHERE P.picture_url = ad;
END
//


CREATE PROCEDURE `insertCustomer`(IN userID INT)
BEGIN
	DECLARE var INT DEFAULT 0;
	SELECT count(*) INTO var FROM Customers C where C.user_id = userID;
	IF (var < 1) THEN \
		INSERT INTO Customers(user_id) VALUES (userID);
	END IF;	
END
//

CREATE PROCEDURE `retrieveAllPSD` ()
BEGIN
	SELECT PSD.price, P.manufacturer, U.first_name, U.user_id
	FROM ProductsSalesData PSD,  Products P, Users U, Customers C
	WHERE PSD.customer_id = U.user_id and PSD.product_id = P.product_id
	GROUP BY PSD.transaction_id
	ORDER BY U.user_id;
END 
//

CREATE PROCEDURE `retrieveAllPsort` ()
BEGIN
	SELECT PSD.price, P.manufacturer, U.first_name, U.user_id, P.product_id
	FROM ProductsSalesData PSD,  Products P, Users U, Customers C
	WHERE PSD.customer_id = U.user_id and PSD.product_id = P.product_id
	GROUP BY PSD.transaction_id
	ORDER BY P.product_id;
END 
//

CREATE PROCEDURE `getCustomerKeywords` (IN userID INT)
BEGIN
	SELECT U.keywords FROM Users U WHERE U.user_id = userID;
END
//

CREATE PROCEDURE `getAdKeywords` (IN adName varchar(50))
BEGIN
	SELECT P.keywords_list FROM Products P WHERE P.picture_url = adName;
END
//

CREATE PROCEDURE `updateUserKeywords` (IN userID INT, IN newKeywords varchar(1500))
BEGIN
	UPDATE Users
	SET keywords=newKeywords
	WHERE user_id = userID;
END
//

CREATE PROCEDURE `gettotalThreadsVotes` (IN threadID INT)
BEGIN 
	SELECT overall_votes FROM Threads where thread_id = threadID;
END 
//

CREATE PROCEDURE `gettotalpostVotes` (IN postID INT)
BEGIN 
	SELECT votes FROM Posts where post_id = postID;
END 
//
 
delimiter ;